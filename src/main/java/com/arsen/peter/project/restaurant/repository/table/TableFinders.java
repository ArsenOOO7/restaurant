package com.arsen.peter.project.restaurant.repository.table;

import com.arsen.peter.project.restaurant.repository.table.find.TableFind;
import com.arsen.peter.project.restaurant.repository.table.find.TableFindById;
import com.arsen.peter.project.restaurant.repository.table.find.TableFindBySeats;
import com.arsen.peter.project.restaurant.repository.table.find.TableFindBySeatsRange;

import java.util.function.Supplier;

public enum TableFinders{

    TABLE_FIND_ID(TableFindById::new),
    TABLE_FIND_SEATS(TableFindBySeats::new),
    TABLE_FIND_SEATS_RANGE(TableFindBySeatsRange::new);

    private final Supplier<TableFind> constructor;

    TableFinders(Supplier<TableFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<TableFind> getConstructor() {
        return constructor;
    }

}
