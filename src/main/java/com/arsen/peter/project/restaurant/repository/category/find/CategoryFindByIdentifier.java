package com.arsen.peter.project.restaurant.repository.category.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class CategoryFindByIdentifier extends CategoryFind{
    @Override
    public Request getRequest() {
        IRequestSelect requestSelect = new RequestSelect();
        requestSelect.setTable(DBTable.CATEGORIES);
        requestSelect.addSelectionItem("*");
        requestSelect.addWhereItem("identifier", map.getString("identifier"));

        return requestSelect;
    }
}
