package com.arsen.peter.project.restaurant.command.defaults;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandFormatter;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;
import com.arsen.peter.project.restaurant.service.data.email.Email;

import java.sql.Date;

public class SettingsCommand extends Command {

    public SettingsCommand(){
        super("settings");
        addSubCommand(new SetSubCommand());
        addSubCommand(new ShowSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return false;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            System.out.println("Usage: settings [set/show]");
            return false;
        }

        String subArg = args[0];

        SubCommand subCommand = getSubCommand(subArg);

        if(subCommand == null){
            System.out.println("Usage: settings [set/show]");
            return false;
        }

        String[] subArgs = CommandFormatter.format(args).getArgs();
        subCommand.executeSub(subArgs);

        return true;
    }


    private static class SetSubCommand implements SubCommand {
        @Override
        public String getLabel() {
            return "set";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 2){
                System.out.println("Usage: settings set [setting] [value]");
                return false;
            }

            User user = Application.CURRENT_USER;

            String setting = args[0];
            String value = args[1];

            switch(setting){
                case "name" -> user.setName(value);
                case "surname" -> user.setSurname(value);
                case "email" -> {

                    if(!Email.isValid(value)){
                        System.out.println("Email is not valid!");
                        return false;
                    }

                    if(Email.emailExists(value)){
                        System.out.println("Email already exists");
                        return false;
                    }

                    user.setEmail(value);

                }
                case "birth" -> {

                    String date_pattern = "\\d{4}-\\d{2}-\\d{2}";
                    if(!value.matches(date_pattern)){
                        System.out.println("Format: yyyy-mm-dd");
                        return false;
                    }

                    user.setBirth_date(Date.valueOf(value));

                }
            }

            System.out.println("You've updated your account");
            UserRepository.getInstance().update(user);

            return true;
        }
    }

    private static class ShowSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "show";
        }

        @Override
        public boolean executeSub(String[] args) {

            User user = Application.CURRENT_USER;

            String name = user.getName().equals("") ? "None" : user.getName();
            String surname = user.getSurname().equals("") ? "None" : user.getSurname();
            String email = user.getEmail().equals("") ? "None" : user.getEmail();
            String date = user.getBirth_date().equals(new Date(0)) ? "None" : user.getBirth_date().toString();

            String print =
                    "Your name: " + name + "\n"
                    + "Your surname: " + surname + "\n"
                    + "Your email: " + email + "\n"
                    + "Your birth: " + date;

            System.out.println(print);
            return true;
        }
    }
}
