package com.arsen.peter.project.restaurant.database.table;

public enum DBTable {

    TEST("test"),

    USERS("users"),
    USERS_BLOCK("users_block"),
    USERS_VIEW("users_view"),

    TABLES("tables"),
    DISHES("dishes"),
    CATEGORIES("categories"),

    BOOKING("booking"),
    BOOKING_DISHES("booking_dishes"),
    USER_ORDERS_VIEW("users_order_view"),

    UNDEFINED("");

    private String table;

    DBTable(String table) {
        this.table = table;
    }

    public String getTable() {
        return table;
    }


    public static DBTable getInstance(String tableTitle){
        DBTable table = DBTable.UNDEFINED;
        for (DBTable value : DBTable.values()) {
            if(value.getTable().equalsIgnoreCase(tableTitle)){
                table = value;
            }
        }
        return table;
    }
}
