package com.arsen.peter.project.restaurant.service.container.map;

import org.json.JSONArray;
import org.json.JSONException;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;

public class CustomHashMap extends HashMap<String, Object> implements IMap {
    @Override
    public String getString(String key) {
        Object obj = get(key);
        return (obj instanceof String)
                ? (String) obj
                : String.valueOf(obj);
    }

    @Override
    public Integer getInt(String key) {
        Object obj = get(key);
        if(obj instanceof Number){
            return (Integer) obj;
        }

        try{
            return Integer.parseInt(getString(key));
        }catch (NumberFormatException exception){
            exception.printStackTrace();
        }

        return 0;
    }

    @Override
    public Double getDouble(String key) {
        Object obj = get(key);
        if(obj instanceof Number){
            return (Double) obj;
        }

        try{
            return Double.parseDouble(getString(key));
        }catch (NumberFormatException exception){
            exception.printStackTrace();
        }

        return 0.0;
    }

    @Override
    public Byte getByte(String key) {
        Object obj = get(key);
        if(obj instanceof Byte){
            return (Byte) obj;
        }

        try{
            return Byte.parseByte(String.valueOf(obj));
        }catch (NumberFormatException exception){
            return 0;
        }
    }

    @Override
    public BigInteger getBigInteger(String key) {
        Object obj = get(key);
        if(obj instanceof BigInteger){
            return (BigInteger) obj;
        }

        try{
            return new BigInteger(getString(key));
        }catch (NumberFormatException exception){
            exception.printStackTrace();
        }

        return BigInteger.ZERO;
    }

    @Override
    public Boolean getBoolean(String key) {
        Object obj = get(key);
        if(obj instanceof Boolean){
            return (Boolean) obj;
        }

        return Boolean.valueOf(String.valueOf(obj));
    }

    @Override
    public Date getDate(String key) {
        Object obj = get(key);
        if(obj instanceof Date){
            return (Date) obj;
        }

        return new Date(0);
    }

    @Override
    public Timestamp getTimestamp(String key) {
        Object obj = get(key);
        if(obj instanceof Timestamp){
            return (Timestamp) obj;
        }

        return new Timestamp(0);
    }

    @Override
    public JSONArray getJSONArray(String key) {
        try {
            return new JSONArray(getString(key));
        }catch (JSONException exception){
            return new JSONArray();
        }
    }
}
