package com.arsen.peter.project.restaurant.command.defaults.admin;

import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandFormatter;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.block.BlockRepository;
import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;

public class UserCommand extends Command {

    public UserCommand() {
        super("user");
        addSubCommand(new GetSubCommand());
        addSubCommand(new BanSubCommand());
        addSubCommand(new EditSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return true;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            System.out.println("Usage: user [get/ban/edit]");
            return false;
        }

        String sub = args[0];
        SubCommand subCommand = getSubCommand(sub);

        if(subCommand == null){
            System.out.println("Usage: user [get/ban/edit]");
            return false;
        }

        subCommand.executeSub(CommandFormatter.format(args).getArgs());

        return true;
    }

    private static class GetSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "get";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                //Maybe it would be better to search by different criteria.
                //F.i: by name, login, id, surname....
                System.out.println("Usage: user get [id]");
                return false;
            }

            String valueUserId = args[0];
            try {

                int userId = Integer.parseInt(valueUserId);
                User user = UserRepository.getInstance().getFinder(UserFinders.USER_FIND_ID)
                        .add("id", userId)
                        .findOne();

                if(user == null){
                    System.out.println("There is no user with such id");
                    return false;
                }

                String userInfo =
                        "Id: " + user.getId() + "\n"
                        + "Name: " + user.getName() + "\n"
                        + "Surname: " + user.getSurname() + "\n"
                        + "Email: " + user.getEmail() + "\n"
                        + "Balance: " + user.getBalance() + "\n"
                        + "Login: " + user.getLogin() + "\n"
                        + "Status: " + user.getStatus().getStatus() + "\n"
                        + "Banned: " + (user.isBlocked() ? " Yes" : " No") + "\n";

                System.out.println(userInfo);

            }catch (NumberFormatException formatException){
                System.out.println("User id must be number!");
            }

            return true;
        }
    }
    private static class BanSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "ban";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 3){
                System.out.println("Usage: user ban [user_id] [reason] [duration (hours)]");
                return false;
            }

            String valueUserId = args[0];
            String reason = args[1];
            String valueDuration = args[2];

            try{

                int userId = Integer.parseInt(valueUserId);
                int duration = Integer.parseInt(valueDuration);

                duration = Math.abs(duration);
                if(duration == 0){
                    System.out.println("Duration must be > 0");
                    return true;
                }

                User user = UserRepository.getInstance().getFinder(UserFinders.USER_FIND_ID)
                        .add("id", userId)
                        .findOne();

                if(user == null){
                    System.out.println("There is no user with such id");
                    return false;
                }

                Block block = new Block();
                block.setUser_id(userId);
                block.setReason(reason);
                block.setEnd_time(duration * 60);

                BlockRepository.getInstance().add(block);
                System.out.println("You've banned user");

            }catch (NumberFormatException formatException){
                System.out.println("");
            }

            return true;
        }
    }


    private static class EditSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "edit";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 3){
                System.out.println("Usage: user edit [user_id] [param] [value]");
                return false;
            }

            String param = args[1];
            String value = args[2];

            int userId = 0;

            try{
                userId = Integer.parseInt(args[0]);
            }catch (NumberFormatException exception){
                System.out.println("User id must be int!");
                return false;
            }

            User user = UserRepository.getInstance().getFinder(UserFinders.USER_FIND_ID)
                    .add("id", userId)
                    .findOne();

            if(user == null){
                System.out.println("There is no user with such id!");
                return false;
            }

            switch (param){
                case "password" -> user.setPassword(value);
                case "setmoney" -> {

                    try{

                        int money = Integer.parseInt(value);
                        user.setBalance(money);

                    }catch (NumberFormatException exception){
                        System.out.println("Money value must be number!");
                        return false;
                    }

                }
                default -> System.out.println("Params: password, setmoney");
            }

            UserRepository.getInstance().add(user);
            System.out.println("You've updated the user");

            return true;
        }
    }

}
