package com.arsen.peter.project.restaurant.service.container.list;

import org.json.JSONArray;
import org.json.JSONException;

import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;

public class CustomArrayList extends ArrayList<Object> implements IList {


    @Override
    public String getString(int key) {
        Object obj = get(key);
        return (obj instanceof String)
                ? (String) obj
                : String.valueOf(obj);
    }

    @Override
    public Integer getInt(int key) {
        Object obj = get(key);
        if(obj instanceof Number){
            return (Integer) obj;
        }

        try{
            return Integer.parseInt(String.valueOf(key));
        }catch (NumberFormatException exception){
            return 0;
        }
    }

    @Override
    public Double getDouble(int key) {
        Object obj = get(key);
        if(obj instanceof Number){
            return (Double) obj;
        }

        try{
            return Double.parseDouble(String.valueOf(key));
        }catch (NumberFormatException exception){
            return 0.0;
        }
    }

    @Override
    public BigInteger getBigInteger(int key) {
        Object obj = get(key);
        if(obj instanceof BigInteger){
            return (BigInteger) obj;
        }

        try{
            return new BigInteger(String.valueOf(key));
        }catch (NumberFormatException exception){
            return BigInteger.ZERO;
        }
    }

    @Override
    public Byte getByte(int key) {
        Object obj = get(key);
        if(obj instanceof Byte){
            return (Byte) obj;
        }

        try{
            return Byte.parseByte(String.valueOf(obj));
        }catch (NumberFormatException exception){
            return 0;
        }
    }

    @Override
    public Boolean getBoolean(int key) {
        Object obj = get(key);
        if(obj instanceof Boolean){
            return (Boolean) obj;
        }

        return Boolean.valueOf(String.valueOf(obj));
    }

    @Override
    public Date getDate(int key) {
        Object obj = get(key);
        if(obj instanceof Date){
            return (Date) obj;
        }

        return new Date(0);
    }

    @Override
    public JSONArray getJSONArray(int key) {
        try {
            return new JSONArray(getString(key));
        }catch (JSONException exception){
            return new JSONArray();
        }
    }
}
