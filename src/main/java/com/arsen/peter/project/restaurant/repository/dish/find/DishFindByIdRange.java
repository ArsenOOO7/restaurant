package com.arsen.peter.project.restaurant.repository.dish.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

import java.util.List;

public class DishFindByIdRange extends DishFind{

    @Override
    public Request getRequest() {

        IRequestSelect select = new RequestSelect();
        select.setTable(DBTable.DISHES);
        select.addSelectionItem("*");


        List<Object> ids = (List<Object>) map.get("list");
        System.out.println(ids);
        select.addInRangeItem("id", (List<Object>) map.get("list"));

        return select;
    }
}
