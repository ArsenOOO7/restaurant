package com.arsen.peter.project.restaurant.database.request.update;

import com.arsen.peter.project.restaurant.database.request.Request;

public interface IRequestUpdate extends Request {

    void addUpdateItem(String key, Object value);
    void addWhereItem(String key, Object value);

}
