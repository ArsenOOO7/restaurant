package com.arsen.peter.project.restaurant.service.data.user;

public enum UserStatus {
    UNDEFINED(-1),
    USER(0),
    ADMIN(1);

    UserStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    private int status;
    public static UserStatus getStatus(int value) {
        for(UserStatus userStatus: UserStatus.values()){
            if(userStatus.getStatus() == value){
                return userStatus;
            }
        }
        return UserStatus.UNDEFINED;
    }
 }
