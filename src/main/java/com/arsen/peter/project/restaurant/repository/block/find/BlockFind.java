package com.arsen.peter.project.restaurant.repository.block.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class BlockFind implements Finder<Block> {

    protected IMap map = new CustomHashMap();

    @Override
    public BlockFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public BlockFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public Block findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<Block> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<Block> list = new ArrayList<>();

        for(IMap item: content){
            Block block = new Block();
            block.extract(item);
            list.add(block);
        }

        return list;
    }

}
