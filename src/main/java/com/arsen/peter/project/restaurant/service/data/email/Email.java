package com.arsen.peter.project.restaurant.service.data.email;

import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;

public class Email {

    public static final String EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";

    public static boolean isValid(String email){
        return email.matches(EMAIL_PATTERN);
    }

    public static boolean emailExists(String email){
        return UserRepository.getInstance().getFinder(UserFinders.USER_FIND_EMAIL)
                .add("email", email)
                .findOne() != null;
    }

}
