package com.arsen.peter.project.restaurant.database.request.update;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.item.update.UpdateItem;
import com.arsen.peter.project.restaurant.database.item.where.WhereItem;
import com.arsen.peter.project.restaurant.database.request.Query;
import com.arsen.peter.project.restaurant.database.table.DBTable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RequestUpdate implements IRequestUpdate{

    private List<RequestItem> updateValues = new ArrayList<>();
    private List<RequestItem> whereValues = new ArrayList<>();

    private DBTable table;

    @Override
    public void setTable(DBTable table) {
        this.table = table;
    }

    @Override
    public void addUpdateItem(String key, Object value){
        updateValues.add(new UpdateItem(key, value));
    }
    @Override
    public void addWhereItem(String key, Object value){
        whereValues.add(new WhereItem(key, value));
    }

    @Override
    public String build() {

        String update = updateValues.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        String where = whereValues.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        String build = Query.UNIQUE_UPDATE
                .replaceFirst("\\?", table.getTable())
                .replaceFirst("\\?", update)
                .replaceFirst("\\?", where);

        return build.trim();
    }
}
