package com.arsen.peter.project.restaurant.repository.table.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class TableFind implements Finder<Table> {

    protected IMap map = new CustomHashMap();

    @Override
    public TableFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public TableFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public Table findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<Table> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<Table> list = new ArrayList<>();

        for(IMap item: content){
            Table table = new Table();
            table.extract(item);
            list.add(table);
        }

        return list;
    }
}
