package com.arsen.peter.project.restaurant.repository.user.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class UserFind implements Finder<User> {

    protected IMap map = new CustomHashMap();

    @Override
    public UserFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public UserFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public User findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<User> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<User> list = new ArrayList<>();

        for(IMap item: content){
            User user = new User();
            user.extract(item);
            list.add(user);
        }

        return list;
    }
}
