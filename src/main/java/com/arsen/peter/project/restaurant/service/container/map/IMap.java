package com.arsen.peter.project.restaurant.service.container.map;

import org.json.JSONArray;

import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Map;

public interface IMap extends Map<String, Object> {

    String getString(String key);
    Integer getInt(String key);
    Double getDouble(String key);
    Byte getByte(String key);
    BigInteger getBigInteger(String key);

    Boolean getBoolean(String key);
    Date getDate(String key);
    Timestamp getTimestamp(String key);
    JSONArray getJSONArray(String key);

}
