package com.arsen.peter.project.restaurant.command.defaults;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandFormatter;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;

import java.util.Map;

public class BalanceCommand extends Command {

    public BalanceCommand() {
        super("balance");
        addSubCommand(new TopUpSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return false;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            User user = Application.CURRENT_USER;
            System.out.println("Your balance: " + user.getBalance());
            return true;
        }

        String sub = args[0];
        SubCommand topUp = getSubCommand(sub);

        if(topUp == null){
            System.out.println("Usage: balance [topup]");
            return false;
        }

        topUp.executeSub(CommandFormatter.format(args).getArgs());

        return true;
    }

    private static class TopUpSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "topup";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                System.out.println("Usage: balance topup [money]");
                return false;
            }

            String value = args[0];

            try{

                int money = Integer.parseInt(value);
                money = Math.abs(money);

                if(money == 0){
                    System.out.println("Amount must be > 0!");
                    return false;
                }

                User user = Application.CURRENT_USER;
                user.addMoney(money);

                System.out.println("You've successfully topped up your balance!");
                UserRepository.getInstance().update(user);


            }catch (NumberFormatException exception){
                System.out.println("Money must be number!");
            }

            return false;
        }
    }

}
