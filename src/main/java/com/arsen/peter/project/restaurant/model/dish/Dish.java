package com.arsen.peter.project.restaurant.model.dish;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

public class Dish implements Model {

    private int id;
    private int category_id;

    private String identifier;

    private int price;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public void extract(IMap result) {
        setId(result.getInt("id"));
        setCategory_id(result.getInt("category_id"));
        setIdentifier(result.getString("identifier"));
        setPrice(result.getInt("price"));
    }
}
