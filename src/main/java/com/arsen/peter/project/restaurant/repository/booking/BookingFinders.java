package com.arsen.peter.project.restaurant.repository.booking;

import com.arsen.peter.project.restaurant.repository.booking.find.BookingFind;
import com.arsen.peter.project.restaurant.repository.booking.find.BookingFindById;
import com.arsen.peter.project.restaurant.repository.booking.find.BookingFindByTableId;
import com.arsen.peter.project.restaurant.repository.booking.find.BookingFindByUserId;

import java.util.function.Supplier;

public enum BookingFinders{

    BOOKING_FIND_ID(BookingFindById::new),
    BOOKING_FIND_USER_ID(BookingFindByUserId::new),
    BOOKING_FIND_TABLE_ID(BookingFindByTableId::new);

    private final Supplier<BookingFind> constructor;

    BookingFinders(Supplier<BookingFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<BookingFind> getConstructor() {
        return constructor;
    }

}
