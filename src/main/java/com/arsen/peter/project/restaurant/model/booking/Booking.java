package com.arsen.peter.project.restaurant.model.booking;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.sql.Timestamp;

public class Booking implements Model {

    private int id;
    private int user_id;
    private int table_id;

    private Timestamp date;
    private BookingDish bookingDish = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTable_id() {
        return table_id;
    }

    public void setTable_id(int table_id) {
        this.table_id = table_id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public BookingDish getBookingDish() {
        return bookingDish;
    }

    public void setBookingDish(BookingDish bookingDish) {
        this.bookingDish = bookingDish;
    }

    public boolean hasBookedAnyDish(){
        return bookingDish != null;
    }


    @Override
    public void extract(IMap result) {
       setId(result.getInt("id"));
       setUser_id(result.getInt("user_id"));
       setTable_id(result.getInt("table_id"));
       setDate(result.getTimestamp("date"));
       if(result.getInt("booking_dishes_id")>=0){
           bookingDish = new BookingDish();
           bookingDish.setId(result.getInt("booking_dishes_id"));
           bookingDish.setUser_id(user_id);
           bookingDish.setBooking_id(id);
           bookingDish.setDishes(result.getJSONArray("dishes"));
       }
    }
}
