package com.arsen.peter.project.restaurant.database.request;

public class Query {

    public static final String UNIQUE_INSERT = "INSERT INTO ? VALUES(DEFAULT, ?)";
    public static final String UNIQUE_UPDATE = "UPDATE ? SET ? WHERE ?";
    public static final String UNIQUE_SELECT = "SELECT ? FROM ? WHERE ?";
    public static final String UNIQUE_SELECT_WITHOUT_WHERE = "SELECT ? FROM ?";
    public static final String UNIQUE_REMOVE = "DELETE FROM ?";

    public static final String RANGE = "? >= %d AND ? <= %d";
    public static final String WHERE = "%s=%s";
    public static final String SET = "%s=%s";
    public static final String LIKE = "%s LIKE %s";

    public static final String LIMIT = "LIMIT ?";
    public static final String OFFSET = "OFFSET ?";

    public static final String SORT_ORDER_BY = "ORDER BY ";


}
