package com.arsen.peter.project.restaurant.repository.block;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.repository.block.find.BlockFind;
import com.arsen.peter.project.restaurant.repository.interfaces.IBlockRepository;

public class BlockRepository implements IBlockRepository {
    private static BlockRepository instance;
    public static BlockRepository getInstance(){
        if(instance == null){
            instance = new BlockRepository();
        }
        return instance;
    }

    private BlockRepository(){}
    @Override
    public void add(Block model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.USERS_BLOCK);
        insert.addItem(model.getUser_id());
        insert.addItem(model.getReason());
        insert.addItem("UNIX_TIMESTAMP()");
        insert.addItem("UNIX_TIMESTAMP() + " + model.getEnd_time());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(Block model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.USERS_BLOCK);

        update.addUpdateItem("user_id", model.getUser_id());
        update.addUpdateItem("reason", model.getReason());
        update.addUpdateItem("start_time", model.getStart_time());
        update.addUpdateItem("end_time", model.getEnd_time());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(Block model) {
        IRequestDelete requestDelete = new RequestDelete();
        requestDelete.setTable(DBTable.USERS_BLOCK);
        requestDelete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(requestDelete);
    }

    @Override
    public BlockFind getFinder(BlockFinders type) {
        return type.getConstructor().get();
    }
}
