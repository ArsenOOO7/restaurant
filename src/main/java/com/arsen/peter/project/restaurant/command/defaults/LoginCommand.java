package com.arsen.peter.project.restaurant.command.defaults;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.interfaces.IUserRepository;
import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;
import com.arsen.peter.project.restaurant.service.data.email.Email;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LoginCommand extends Command {


    public LoginCommand() {
        super("login");
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length != 2){
            System.out.println("Usage: login [login] [password]");
            return false;
        }

        String login = args[0];
        String password = args[1];

        UserFinders userFind;
        String loginType;

        IUserRepository userRepository = UserRepository.getInstance();

        if(Email.isValid(login)){
            userFind = UserFinders.USER_FIND_EMAIL;
            loginType = "email";
        }else{
            userFind = UserFinders.USER_FIND_LOGIN;
            loginType = "login";
        }

        User user = userRepository.getFinder(userFind)
                .add(loginType, login)
                .findOne();

        if(user == null){
            System.out.println("Incorrect login!");
            return false;
        }

        if(!user.getPassword().equals(password)){
            System.out.println("Incorrect password");
            return false;
        }

        if(user.isBlocked()){
            Block block = user.getBlock();
            String reason = block.getReason();

            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss MM-dd-yyyy");
            String until_date = dateFormat.format(new Date(block.getEnd_time() * 1000L));

            System.out.println("You're banned. Reason: " + reason + "\nTill: " + until_date);
            return false;

        }

        Application.CURRENT_USER = user;
        System.out.println("You have successfully signed in");

        return true;
    }

    @Override
    public boolean requireAdmin() {
        return false;
    }

    @Override
    public boolean isAuthNeed() {
        return false;
    }
}
