package com.arsen.peter.project.restaurant.model.block;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

public class Block implements Model {

    private int id;
    private int user_id;

    private String reason;

    private int start_time;
    private int end_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getStart_time() {
        return start_time;
    }

    public void setStart_time(int start_time) {
        this.start_time = start_time;
    }

    public int getEnd_time() {
        return end_time;
    }

    public void setEnd_time(int end_time) {
        this.end_time = end_time;
    }


    @Override
    public void extract(IMap result) {
        setId(result.getInt("id"));
        setUser_id(result.getInt("user_id"));
        setReason(result.getString("reason"));
        setStart_time(result.getInt("start_time"));
        setEnd_time(result.getInt("end_time"));

    }
}
