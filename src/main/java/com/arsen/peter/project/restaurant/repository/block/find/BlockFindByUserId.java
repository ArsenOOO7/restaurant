package com.arsen.peter.project.restaurant.repository.block.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class BlockFindByUserId extends BlockFind{
    @Override
    public Request getRequest() {
        IRequestSelect select = new RequestSelect();
        select.setTable(DBTable.USERS_BLOCK);
        select.addSelectionItem("*");
        select.addWhereItem("user_id", map.getInt("user_id"));

        return select;
    }
}
