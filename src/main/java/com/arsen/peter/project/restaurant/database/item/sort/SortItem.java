package com.arsen.peter.project.restaurant.database.item.sort;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.item.sort.order.SortingOrder;

public class SortItem implements RequestItem {

    private String orderBy;
    private SortingOrder order;

    public SortItem(String orderBy, SortingOrder order) {
        this.orderBy = orderBy;
        this.order = order;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public SortingOrder getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return orderBy + " " + order.toString();
    }
}
