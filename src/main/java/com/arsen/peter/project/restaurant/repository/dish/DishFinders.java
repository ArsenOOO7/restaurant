package com.arsen.peter.project.restaurant.repository.dish;

import com.arsen.peter.project.restaurant.repository.dish.find.*;

import java.util.function.Supplier;

public enum DishFinders {

    DISH_FIND_ID(DishFindById::new),
    DISH_FIND_CATEGORY(DishFindByCategory::new),
    DISH_FIND_PRICE_RANGE(DishFindByPriceRange::new),
    DISH_FIND_IN_RANGE(DishFindByIdRange::new),
    DISH_FIND_IDENTIFIER(DishFindByIdentifier::new);

    private final Supplier<DishFind> constructor;

    DishFinders(Supplier<DishFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<DishFind> getConstructor() {
        return constructor;
    }

}
