package com.arsen.peter.project.restaurant.repository.dish;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.dish.find.DishFind;
import com.arsen.peter.project.restaurant.repository.interfaces.IDishRepository;

public class DishRepository implements IDishRepository {
    private static DishRepository instance;
    public static DishRepository getInstance(){
        if(instance == null){
            instance = new DishRepository();
        }
        return instance;
    }

    private DishRepository(){}
    @Override
    public void add(Dish model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.DISHES);
        insert.addItem(model.getCategory_id());
        insert.addItem(model.getIdentifier());
        insert.addItem(model.getPrice());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(Dish model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.DISHES);
        update.addUpdateItem("category_id", model.getCategory_id());
        update.addUpdateItem("idenfitier", model.getIdentifier());
        update.addUpdateItem("price", model.getPrice());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(Dish model) {
        IRequestDelete delete = new RequestDelete();
        delete.setTable(DBTable.DISHES);
        delete.addWhereItem("id", model.getId());
    }

    @Override
    public DishFind getFinders(DishFinders type) {
        return type.getConstructor().get();
    }
}
