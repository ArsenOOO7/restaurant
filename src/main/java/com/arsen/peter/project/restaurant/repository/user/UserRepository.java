package com.arsen.peter.project.restaurant.repository.user;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.interfaces.IUserRepository;
import com.arsen.peter.project.restaurant.repository.user.find.UserFind;

public class UserRepository implements IUserRepository {


    private static UserRepository instance;
    public synchronized static UserRepository getInstance(){
        if(instance == null){
            instance = new UserRepository();
        }
        return instance;
    }

    private UserRepository(){}

    @Override
    public void add(User model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.USERS);
        insert.addItem(model.getName());
        insert.addItem(model.getSurname());
        insert.addItem(model.getEmail());
        insert.addItem(model.getBirth_date());
        insert.addItem(model.getBalance());
        insert.addItem(model.getStatus().getStatus());
        insert.addItem(model.getLogin());
        insert.addItem(model.getPassword());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(User model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.USERS);
        update.addUpdateItem("name", model.getName());
        update.addUpdateItem("surname", model.getSurname());
        update.addUpdateItem("email", model.getEmail());
        update.addUpdateItem("login", model.getLogin());
        update.addUpdateItem("password", model.getPassword());
        update.addUpdateItem("balance", model.getBalance());
        update.addUpdateItem("status", model.getStatus().getStatus());
        update.addUpdateItem("birth_date", model.getBirth_date());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(User model) {
        IRequestDelete requestDelete = new RequestDelete();
        requestDelete.setTable(DBTable.USERS);
        requestDelete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(requestDelete);
    }

    @Override
    public UserFind getFinder(UserFinders type){
        return type.getConstructor().get();
    }
}
