package com.arsen.peter.project.restaurant.repository;

import com.arsen.peter.project.restaurant.model.Model;

public interface Repository<T extends Model> {

    void add(T model);
    void update(T model);
    void remove(T model);

}
