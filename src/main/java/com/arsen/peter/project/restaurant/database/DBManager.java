package com.arsen.peter.project.restaurant.database;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.result.ResultList;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {

    private static DBManager instance;
    public synchronized static DBManager getInstance(){
        if(instance == null){
            instance = new DBManager();
        }
        return instance;
    }


    private DBManager(){
        DBConnection.getInstance();
    }


    /**
     *
     * This method gets the Request instance and inserts a new value to database.
     * Returns id of inserted value
     *
     * @param request Request
     * @return int
     */
    public int insertItem(Request request){
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        int id = -1;

        try{

            connection = DBConnection.getInstance().getConnection();
            connection.setAutoCommit(false);

            statement = connection.createStatement();
            statement.executeUpdate(request.build(), Statement.RETURN_GENERATED_KEYS);
            resultSet = statement.getGeneratedKeys();

            if(resultSet.next()){
                id = resultSet.getInt(1);
            }

            connection.commit();
        }catch (SQLException exception){
            DBConnection.rollback(connection);
            exception.printStackTrace();
        }finally {
            DBConnection.close(resultSet, statement, connection);
        }

        return id;
    }


    public void updateItem(Request request){
        Connection connection = null;
        Statement statement = null;
        try {
            connection = DBConnection.getInstance().getConnection();
            connection.setAutoCommit(false);

            statement = connection.createStatement();
            statement.executeUpdate(request.build());

            connection.commit();
        }catch (SQLException exception){
            DBConnection.rollback(connection);
            exception.printStackTrace();
        }finally {
            DBConnection.close(statement, connection);
        }
    }


    public ResultList selectItems(Request request){
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        ResultList resultList = new ResultList();
        try{
            connection = DBConnection.getInstance().getConnection();
            connection.setAutoCommit(false);

            statement = connection.createStatement();
            resultSet = statement.executeQuery(request.build());
            resultList.setContent(resultSet);

            connection.commit();
        }catch (SQLException exception){
            DBConnection.rollback(connection);
            exception.printStackTrace();
        }finally {
            DBConnection.close(resultSet, statement, connection);
        }

        return resultList;
    }


    public void removeItems(Request request){
        Connection connection = null;
        Statement statement = null;
        try{
            connection = DBConnection.getInstance().getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            statement.executeUpdate(request.build());

            connection.commit();
        }catch (SQLException exception){
            DBConnection.rollback(connection);
        }finally {
            DBConnection.close(statement, connection);
        }
    }


}
