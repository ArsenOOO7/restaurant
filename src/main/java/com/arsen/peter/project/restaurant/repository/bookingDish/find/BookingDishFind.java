package com.arsen.peter.project.restaurant.repository.bookingDish.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class BookingDishFind implements Finder<BookingDish> {

    protected IMap map = new CustomHashMap();

    @Override
    public BookingDishFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public BookingDishFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public BookingDish findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<BookingDish> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<BookingDish> list = new ArrayList<>();

        for(IMap item: content){
            BookingDish bookingDish = new BookingDish();
            bookingDish.extract(item);
            list.add(bookingDish);
        }

        return list;
    }

}
