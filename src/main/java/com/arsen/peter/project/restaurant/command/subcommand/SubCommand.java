package com.arsen.peter.project.restaurant.command.subcommand;

public interface SubCommand {

    String getLabel();
    boolean executeSub(String[] args);

}
