package com.arsen.peter.project.restaurant.database.request.delete;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.item.where.WhereItem;
import com.arsen.peter.project.restaurant.database.request.Query;
import com.arsen.peter.project.restaurant.database.table.DBTable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RequestDelete implements IRequestDelete {

    private List<RequestItem> list = new ArrayList<>();
    private DBTable table;


    @Override
    public void setTable(DBTable table) {
        this.table = table;
    }

    @Override
    public void addWhereItem(String key, Object value) {
        list.add(new WhereItem(key, value));
    }

    @Override
    public String build() {
        String whereItems = list.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        String removeQuery = Query.UNIQUE_REMOVE;
        removeQuery = removeQuery.replaceFirst("\\?", table.getTable());

        if(list.size() > 0){

            removeQuery = String.join(" ", removeQuery, "WHERE", whereItems);

        }

        return removeQuery;
    }
}
