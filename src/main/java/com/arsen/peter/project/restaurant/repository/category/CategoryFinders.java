package com.arsen.peter.project.restaurant.repository.category;

import com.arsen.peter.project.restaurant.repository.category.find.CategoryFind;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFindAll;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFindById;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFindByIdentifier;

import java.util.function.Supplier;

public enum CategoryFinders {

    CATEGORY_FIND_ID(CategoryFindById::new),
    CATEGORY_FIND_IDENTIFIER(CategoryFindByIdentifier::new),
    CATEGORY_FIND_ALL(CategoryFindAll::new);

    private final Supplier<CategoryFind> constructor;

    CategoryFinders(Supplier<CategoryFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<CategoryFind> getConstructor() {
        return constructor;
    }

}
