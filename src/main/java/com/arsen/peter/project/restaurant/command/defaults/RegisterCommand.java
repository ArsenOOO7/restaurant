package com.arsen.peter.project.restaurant.command.defaults;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.interfaces.IUserRepository;
import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;

import java.sql.Date;

public class RegisterCommand extends Command {


    public RegisterCommand() {
        super("register");
    }


    @Override
    public boolean execute(String[] args) {

        if(Application.CURRENT_USER != null){
            System.out.println("You have been authorized");
            return false;
        }

        if(args.length != 3){
            System.out.println("Usage: register [login] [password] [password repeat]");
            return false;
        }

        String login = args[0];
        String password = args[1];
        String password_repeat = args[2];

        IUserRepository userRepository = UserRepository.getInstance();

        User checkIfExist = userRepository.getFinder(UserFinders.USER_FIND_LOGIN)
                .add("login", login).findOne();

        if(checkIfExist != null){
            System.out.println("User with this login already exists");
            return false;
        }

        if(!password.equals(password_repeat)){
            System.out.println("You have repeated the password incorrectly");
            return false;
        }


        User user = User.createUser("", "", "",
                new Date(0), 0,  login, password, UserStatus.USER);
        
        userRepository.add(user);

        System.out.println("You have successfully registered. Now you can sign in (login cmd)");


       return true;
    }

    @Override
    public boolean requireAdmin() {
        return false;
    }

    @Override
    public boolean isAuthNeed() {
        return false;
    }


}
