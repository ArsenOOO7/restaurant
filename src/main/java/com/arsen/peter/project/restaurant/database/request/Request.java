package com.arsen.peter.project.restaurant.database.request;


import com.arsen.peter.project.restaurant.database.table.DBTable;

public interface Request {

    void setTable(DBTable table);
    String build();

}
