package com.arsen.peter.project.restaurant.database.request.select;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.item.select.SelectItem;
import com.arsen.peter.project.restaurant.database.item.sort.SortItem;
import com.arsen.peter.project.restaurant.database.item.sort.order.SortingOrder;
import com.arsen.peter.project.restaurant.database.item.where.InRangeItem;
import com.arsen.peter.project.restaurant.database.item.where.LikeItem;
import com.arsen.peter.project.restaurant.database.item.where.RangeItem;
import com.arsen.peter.project.restaurant.database.item.where.WhereItem;
import com.arsen.peter.project.restaurant.database.request.Query;
import com.arsen.peter.project.restaurant.database.table.DBTable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RequestSelect implements IRequestSelect{

    private DBTable table;

    private List<RequestItem> selectRequestItems = new ArrayList<>();
    private List<RequestItem> sortRequestItems = new ArrayList<>();
    private List<RequestItem> whereRequestItems = new ArrayList<>();

    private int limit = 0;
    private int offset = 0;

    @Override
    public void setTable(DBTable table) {
        this.table = table;
    }

    @Override
    public void addSelectionItem(String value) {
        selectRequestItems.add(new SelectItem(value));
    }

    @Override
    public void addWhereItem(String key, Object value) {
        whereRequestItems.add(new WhereItem(key, value));
    }

    @Override
    public void addRangeItem(String key, int min, int max) {
        whereRequestItems.add(new RangeItem(key, min, max));
    }

    @Override
    public void addLikeItem(String key, Object like) {
        whereRequestItems.add(new LikeItem(key, like));
    }

    @Override
    public void addInRangeItem(String key, List<Object> values) {
        whereRequestItems.add(new InRangeItem(key, values));
    }

    @Override
    public void addSortItem(String key, SortingOrder order) {
        sortRequestItems.add(new SortItem(key, order));
    }

    @Override
    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String build() {

        String select = selectRequestItems.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        String where = whereRequestItems.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(" AND "));

        String sort = sortRequestItems.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        if(!sort.isEmpty()) {
            sort = Query.SORT_ORDER_BY + sort;
        }

        String limitStr = (limit > 0) ? "LIMIT " + limit : "";
        String offsetStr = (offset != 0) ? "OFFSET " + offset : "";

        String request = Query.UNIQUE_SELECT;
        if(whereRequestItems.size() < 1){
            request = Query.UNIQUE_SELECT_WITHOUT_WHERE;
        }

        String build = request
                .replaceFirst("\\?", select)
                .replaceFirst("\\?", table.getTable())
                .replaceFirst("\\?", where).trim()
                + " "
                + sort
                + " "
                + String.join(" ", limitStr, offsetStr);

        return build.trim();
    }
}
