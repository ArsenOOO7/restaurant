package com.arsen.peter.project.restaurant.repository.dish.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class DishFind implements Finder<Dish> {

    protected IMap map = new CustomHashMap();

    @Override
    public DishFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public DishFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public Dish findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<Dish> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<Dish> list = new ArrayList<>();

        for(IMap item: content){
            Dish dish = new Dish();
            dish.extract(item);
            list.add(dish);
        }

        return list;
    }

}
