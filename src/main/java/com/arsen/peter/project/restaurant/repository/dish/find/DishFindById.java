package com.arsen.peter.project.restaurant.repository.dish.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class DishFindById extends DishFind{
    @Override
    public Request getRequest() {
        IRequestSelect select = new RequestSelect();
        select.setTable(DBTable.DISHES);
        select.addSelectionItem("*");
        select.addWhereItem("id", map.getInt("id"));

        return select;
    }
}
