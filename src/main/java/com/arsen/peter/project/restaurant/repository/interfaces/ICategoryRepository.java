package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.category.CategoryFinders;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFind;

public interface ICategoryRepository extends Repository<Category> {

    CategoryFind getFinders(CategoryFinders type);

}
