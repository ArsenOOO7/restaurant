package com.arsen.peter.project.restaurant.database.request.delete;

import com.arsen.peter.project.restaurant.database.request.Request;

public interface IRequestDelete extends Request {

    void addWhereItem(String key, Object value);

}
