package com.arsen.peter.project.restaurant.database.item.where;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.service.string.SingleQuote;

import java.util.List;
import java.util.stream.Collectors;

public class InRangeItem implements RequestItem {

    private String key;
    private List<Object> list;

    public InRangeItem(String key, List<Object> list){
        this.key = key;
        this.list = list;
    }

    public void addObj(Object obj){
        list.add(obj);
    }

    @Override
    public String toString() {
        return key + " IN (" +
                list.stream()
                        .map(SingleQuote::toSingle)
                        .collect(Collectors.joining(", "))
                + ")";
    }
}
