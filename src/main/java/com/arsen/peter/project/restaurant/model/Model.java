package com.arsen.peter.project.restaurant.model;

import com.arsen.peter.project.restaurant.service.container.map.IMap;

public interface Model {

    void extract(IMap result);

}
