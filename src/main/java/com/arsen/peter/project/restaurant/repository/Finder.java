package com.arsen.peter.project.restaurant.repository;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.List;

public interface Finder<T> {

    Finder<T> add(String key, Object obj);
    Finder<T> add(IMap map);

    T findOne();
    List<T> findAll();

    Request getRequest();
}
