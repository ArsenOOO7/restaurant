package com.arsen.peter.project.restaurant.repository.user;

import com.arsen.peter.project.restaurant.repository.user.find.UserFind;
import com.arsen.peter.project.restaurant.repository.user.find.UserFindByEmail;
import com.arsen.peter.project.restaurant.repository.user.find.UserFindById;
import com.arsen.peter.project.restaurant.repository.user.find.UserFindByLogin;

import java.util.function.Supplier;

public enum UserFinders{

    USER_FIND_ID(UserFindById::new),
    USER_FIND_LOGIN(UserFindByLogin::new),
    USER_FIND_EMAIL(UserFindByEmail::new);

    private final Supplier<UserFind> constructor;

    UserFinders(Supplier<UserFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<UserFind> getConstructor() {
        return constructor;
    }
}
