package com.arsen.peter.project.restaurant.database.item.insert;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.service.string.SingleQuote;

public class InsertItem implements RequestItem {

    private Object value;

    public InsertItem(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return SingleQuote.toSingle(value);
    }
}
