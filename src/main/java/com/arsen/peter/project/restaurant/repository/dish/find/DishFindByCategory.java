package com.arsen.peter.project.restaurant.repository.dish.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class DishFindByCategory extends DishFind{
    @Override
    public Request getRequest() {
        IRequestSelect requestSelect = new RequestSelect();
        requestSelect.setTable(DBTable.DISHES);

        requestSelect.addSelectionItem("*");
        requestSelect.addWhereItem("category_id", map.getInt("category"));

        return requestSelect;
    }
}
