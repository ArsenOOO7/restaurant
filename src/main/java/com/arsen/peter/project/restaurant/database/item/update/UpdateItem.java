package com.arsen.peter.project.restaurant.database.item.update;

import com.arsen.peter.project.restaurant.database.item.where.WhereItem;

public class UpdateItem extends WhereItem {
    public UpdateItem(String key, Object value) {
        super(key, value);
    }
}
