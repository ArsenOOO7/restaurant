package com.arsen.peter.project.restaurant.command.defaults;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;

public class ExitCommand extends Command {

    public ExitCommand() {
        super("exit");
    }

    @Override
    public boolean requireAdmin() {
        return false;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        //Update current user in db
        Application.CURRENT_USER = null;
        System.out.println("You've logged out");

        return true;
    }
}
