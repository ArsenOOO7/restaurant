package com.arsen.peter.project.restaurant.database.request.insert;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.item.insert.InsertItem;
import com.arsen.peter.project.restaurant.database.request.Query;
import com.arsen.peter.project.restaurant.database.table.DBTable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RequestInsert implements IRequestInsert {

    private List<RequestItem> list = new ArrayList<>();
    private DBTable table;

    @Override
    public void setTable(DBTable table){
        this.table = table;
    }

    @Override
    public void addItem(Object item){
        list.add(new InsertItem(item));
    }

    @Override
    public String build() {
        String insertValues = list.stream()
                .map(RequestItem::toString)
                .collect(Collectors.joining(", "));

        return Query.UNIQUE_INSERT
                .replaceFirst("\\?", table.getTable())
                .replaceFirst("\\?", insertValues);
    }

}
