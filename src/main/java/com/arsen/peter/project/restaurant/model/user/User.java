package com.arsen.peter.project.restaurant.model.user;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.repository.block.BlockFinders;
import com.arsen.peter.project.restaurant.repository.block.BlockRepository;
import com.arsen.peter.project.restaurant.repository.block.find.BlockFind;
import com.arsen.peter.project.restaurant.repository.interfaces.IBlockRepository;
import com.arsen.peter.project.restaurant.service.container.map.IMap;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;

import java.sql.Date;

public class User implements Model {

    private int id;

    private String name;
    private String surname;

    private String login;
    private String password;
    private String email;

    private Date birth_date;

    private int balance;
    private UserStatus status;

    private Block block = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void addMoney(int money){
        balance += money;
    }

    public boolean hasEnoughMoney(int sum){
        return balance >= sum;
    }

    public void reduceMoney(int money){
        balance -= money;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public boolean isBlocked(){
        return block != null;
    }

    @Override
    public void extract(IMap result) {
        setName(result.getString("name"));
        setId(result.getInt("id"));
        setSurname(result.getString("surname"));
        setEmail(result.getString("email"));
        setLogin(result.getString("login"));
        setPassword(result.getString("password"));
        setBalance(result.getInt("balance"));
        setBirth_date(result.getDate("birth_date"));
        setStatus(UserStatus.getStatus(result.getInt("status")));

        if(result.getInt("blocked") == 1){
            IBlockRepository blockRepo = BlockRepository.getInstance();

            BlockFind blockFind = blockRepo.getFinder(BlockFinders.BLOCK_FIND_USER_ID);
            blockFind.add("user_id", id);

            this.block = blockFind.findOne();
        }
    }

    public static User createUser(String name, String surname, String email, Date birth, int balance,
                                  String login, String password, UserStatus status){
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setBirth_date(birth);

        user.setBalance(balance);

        user.setLogin(login);
        user.setPassword(password);

        user.setStatus(status);

        return user;
    }

}
