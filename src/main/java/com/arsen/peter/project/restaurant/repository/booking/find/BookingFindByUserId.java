package com.arsen.peter.project.restaurant.repository.booking.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class BookingFindByUserId extends BookingFind{
    @Override
    public Request getRequest() {
        IRequestSelect requestSelect = new RequestSelect();
        requestSelect.setTable(DBTable.USER_ORDERS_VIEW);
        requestSelect.addSelectionItem("*");
        requestSelect.addWhereItem("user_id", map.getInt("user_id"));

        return requestSelect;
    }
}
