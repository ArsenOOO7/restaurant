package com.arsen.peter.project.restaurant.database.item.where;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.request.Query;
import com.arsen.peter.project.restaurant.service.string.SingleQuote;

public class LikeItem implements RequestItem {

    private String key;
    private Object value;

    public LikeItem(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.format(Query.LIKE, key, SingleQuote.toSingle(value + "%"));
    }
}
