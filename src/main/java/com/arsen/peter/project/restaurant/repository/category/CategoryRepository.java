package com.arsen.peter.project.restaurant.repository.category;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFind;
import com.arsen.peter.project.restaurant.repository.interfaces.ICategoryRepository;

public class CategoryRepository implements ICategoryRepository {
    private static CategoryRepository instance;
    public static CategoryRepository getInstance(){
        if(instance == null){
            instance = new CategoryRepository();
        }
        return instance;
    }

    private CategoryRepository(){}
    @Override
    public void add(Category model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.CATEGORIES);
        insert.addItem(model.getIdentifier());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(Category model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.CATEGORIES);
        update.addUpdateItem("identifier", model.getIdentifier());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(Category model) {
        IRequestDelete requestDelete = new RequestDelete();
        requestDelete.setTable(DBTable.CATEGORIES);
        requestDelete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(requestDelete);
    }


    @Override
    public CategoryFind getFinders(CategoryFinders type) {
        return type.getConstructor().get();
    }
}
