package com.arsen.peter.project.restaurant.model.bookingDish;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.dish.DishFinders;
import com.arsen.peter.project.restaurant.repository.dish.DishRepository;
import com.arsen.peter.project.restaurant.repository.interfaces.IDishRepository;
import com.arsen.peter.project.restaurant.service.container.map.IMap;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class BookingDish implements Model {

    private int id;
    private int user_id;
    private int booking_id;
    private List<Dish> dishes = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public void setDishes(JSONArray dishes_id) {
        IDishRepository dishRepository = DishRepository.getInstance();

//        List<Integer> list = dishes_id.toList()
//                .stream()
//                .map(id -> (Integer) id)
//                .toList();

        dishes = dishRepository.getFinders(DishFinders.DISH_FIND_IN_RANGE)
                .add("list", dishes_id.toList())
                .findAll();
    }

    public void addDish(Dish dish){
        dishes.add(dish);
    }

    public int getBooking_id() {return booking_id;}

    public void setBooking_id(int booking_id) {this.booking_id = booking_id;}

    @Override
    public void extract(IMap result) {
        setId(result.getInt("id"));
        setUser_id(result.getInt("user_id"));
        setDishes(result.getJSONArray("dishes_id"));
        setBooking_id(result.getInt("booking_id"));
    }
}
