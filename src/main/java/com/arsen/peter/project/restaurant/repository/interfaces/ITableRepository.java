package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.table.TableFinders;
import com.arsen.peter.project.restaurant.repository.table.find.TableFind;

public interface ITableRepository extends Repository<Table> {

    TableFind getFinders(TableFinders type);

}
