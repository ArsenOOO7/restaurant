package com.arsen.peter.project.restaurant.repository.table;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.repository.interfaces.ITableRepository;
import com.arsen.peter.project.restaurant.repository.table.find.TableFind;

public class TableRepository implements ITableRepository {
    private static TableRepository instance;
    public static TableRepository getInstance(){
        if(instance == null){
            instance = new TableRepository();
        }
        return instance;
    }

    private TableRepository(){}
    @Override
    public void add(Table model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.TABLES);
        insert.addItem(model.getSeats());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(Table model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.TABLES);
        update.addUpdateItem("seats", model.getSeats());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(Table model) {
        IRequestDelete delete = new RequestDelete();
        delete.setTable(DBTable.TABLES);
        delete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(delete);
    }

    @Override
    public TableFind getFinders(TableFinders type) {
        return type.getConstructor().get();
    }
}
