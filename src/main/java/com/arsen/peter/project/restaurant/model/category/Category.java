package com.arsen.peter.project.restaurant.model.category;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

public class Category implements Model {

    private int id;
    private String identifier;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public void extract(IMap result) {
        setId(result.getInt("id"));
        setIdentifier(result.getString("identifier"));
    }
}
