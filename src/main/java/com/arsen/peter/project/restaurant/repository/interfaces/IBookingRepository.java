package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.booking.Booking;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.booking.BookingFinders;
import com.arsen.peter.project.restaurant.repository.booking.find.BookingFind;

public interface IBookingRepository extends Repository<Booking> {

    BookingFind getFinder(BookingFinders type);

}
