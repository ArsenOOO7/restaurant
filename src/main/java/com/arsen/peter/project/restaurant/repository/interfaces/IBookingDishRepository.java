package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.bookingDish.BookingDishFinders;
import com.arsen.peter.project.restaurant.repository.bookingDish.find.BookingDishFind;

public interface IBookingDishRepository extends Repository<BookingDish> {

    BookingDishFind getFinder(BookingDishFinders type);

}
