package com.arsen.peter.project.restaurant.repository.booking.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class BookingFindByTableId extends BookingFind{
    @Override
    public Request getRequest() {
        IRequestSelect select = new RequestSelect();
        select.setTable(DBTable.USER_ORDERS_VIEW);
        select.addSelectionItem("*");
        select.addWhereItem("table_id", map.getInt("table_id"));

        return select;
    }
}
