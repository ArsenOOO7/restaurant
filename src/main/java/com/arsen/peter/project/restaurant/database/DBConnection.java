package com.arsen.peter.project.restaurant.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

    private String address;


    private static DBConnection instance;
    public synchronized static DBConnection getInstance(){
        if(instance == null){
            instance = new DBConnection();
        }
        return instance;
    }


    private DBConnection(){
        getAddress();
    }

    private void getAddress(){
        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream("app.properties")){
            Properties properties = new Properties();
            properties.load(inputStream);

            address = properties.getProperty("database.connection");
            Class.forName(properties.getProperty("database.driver"));

            System.out.println("Connected to DB successfully");
        }catch (IOException | ClassNotFoundException exception){
            exception.printStackTrace();
            System.out.println("Can't connect to DB");
        }
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(address);
    }

    public static void close(AutoCloseable ...closeables){
        try {
            for (AutoCloseable closeable : closeables) {
                if(closeable != null) {
                    closeable.close();
                }
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public static void rollback(Connection connection){
        try{
            if(connection != null) {
                connection.rollback();
            }
        }catch (SQLException exception){
            exception.printStackTrace();
        }
    }

}
