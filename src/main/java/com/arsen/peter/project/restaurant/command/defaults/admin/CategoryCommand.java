package com.arsen.peter.project.restaurant.command.defaults.admin;

import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandFormatter;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.repository.category.CategoryFinders;
import com.arsen.peter.project.restaurant.repository.category.CategoryRepository;
import com.arsen.peter.project.restaurant.repository.interfaces.ICategoryRepository;

import java.util.List;

public class CategoryCommand extends Command {


    public CategoryCommand() {
        super("category");
        addSubCommand(new AddSubCommand());
        addSubCommand(new ListSubCommand());
        addSubCommand(new RemoveSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return true;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            System.out.println("Usage: category [add/list/remove]");
            return false;
        }

        String value = args[0];
        SubCommand subCommand = getSubCommand(value);

        if(subCommand == null){
            System.out.println("Usage: category [add/list/remove]");
            return false;
        }

        subCommand.executeSub(CommandFormatter.format(args).getArgs());

        return true;
    }


    private static class AddSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "add";
        }

        @Override
        public boolean executeSub(String[] args) {
            if(args.length != 1){
                System.out.println("Usage: category add [identifier] (space = 's:')");
                return false;
            }

            String catName = args[0];
            catName = catName.replace(":", " ");

            ICategoryRepository categoryRepository = CategoryRepository.getInstance();

            Category findCategory = categoryRepository.getFinders(CategoryFinders.CATEGORY_FIND_IDENTIFIER)
                    .add("identifier", catName)
                    .findOne();

            if(findCategory != null){
                System.out.println("There is a category with same name!");
                return false;
            }

            Category addCategory = new Category();
            addCategory.setIdentifier(catName);


            categoryRepository.add(addCategory);
            System.out.println("You've added new category");

            return true;
        }
    }


    private static class ListSubCommand implements SubCommand{


        @Override
        public String getLabel() {
            return "list";
        }

        @Override
        public boolean executeSub(String[] args) {

            List<Category> list = CategoryRepository.getInstance().getFinders(CategoryFinders.CATEGORY_FIND_ALL)
                    .findAll();

            StringBuilder builder = new StringBuilder();
            builder.append("ID\tNAME");
            list.forEach(category -> builder.append(category.getId()).append("\t").append(category.getIdentifier())
                    .append("\n"));

            System.out.println(builder);

            return true;
        }
    }


    private static class RemoveSubCommand implements SubCommand{


        @Override
        public String getLabel() {
            return "remove";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                System.out.println("Usage: category remove [id]");
                return false;
            }

            String value = args[0];
            try{

                int id = Integer.parseInt(value);
                Category category = CategoryRepository.getInstance().getFinders(CategoryFinders.CATEGORY_FIND_ID)
                        .add("id", id)
                        .findOne();

                if(category == null){
                    System.out.println("Non-existing category");
                    return false;
                }

                CategoryRepository.getInstance().remove(category);
                System.out.println("You successfully removed category!");

            }catch (NumberFormatException exception){
                System.out.println("Id must be number!");
            }

            return true;
        }
    }

}
