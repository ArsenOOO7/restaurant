package com.arsen.peter.project.restaurant.repository.booking.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.booking.Booking;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class BookingFind implements Finder<Booking> {

    protected IMap map = new CustomHashMap();

    @Override
    public BookingFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public BookingFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public Booking findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<Booking> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<Booking> list = new ArrayList<>();

        for(IMap item: content){
            Booking booking = new Booking();
            booking.extract(item);
            list.add(booking);
        }

        return list;
    }

}
