package com.arsen.peter.project.restaurant.database.result;

import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResultList {

    private final List<IMap> content = new ArrayList<>();

    public void setContent(ResultSet resultSet) throws SQLException {
        while (resultSet.next()){

            ResultSetMetaData metaData = resultSet.getMetaData();
            IMap map = new CustomHashMap();

            for(int i = 1; i <= metaData.getColumnCount(); ++i){
                map.put(metaData.getColumnName(i).toLowerCase(), resultSet.getObject(i));
            }

            content.add(map);

        }
    }

    public List<IMap> getContent() {
        return content;
    }
}
