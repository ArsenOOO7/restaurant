package com.arsen.peter.project.restaurant.service.string;

public class SingleQuote {

    public static String toSingle(Object object){
        String stringValue = String.valueOf(object);

        if(stringValue.isEmpty()){
            return "''";
        }

        return stringValue.length() > 1 && !stringValue.contains("()")
                ? "'" + stringValue + "'"
                : stringValue;
    }

}
