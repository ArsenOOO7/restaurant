package com.arsen.peter.project.restaurant.command;

import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;

import java.util.ArrayList;
import java.util.List;

public abstract class Command {

    private final String label;
    private final List<SubCommand> subCommands = new ArrayList<>();

    public Command(String label){
        this.label = label;
    }

    public String getLabel(){
        return label;
    }

    public void addSubCommand(SubCommand command){
        subCommands.add(command);
    }

    public SubCommand getSubCommand(String subLabel){

        return subCommands.stream()
                .filter(subCommand -> subCommand.getLabel().equals(subLabel))
                .findAny()
                .orElse(null);
    }


    public abstract boolean execute(String[] args);

    public abstract boolean requireAdmin();
    public abstract boolean isAuthNeed();

}
