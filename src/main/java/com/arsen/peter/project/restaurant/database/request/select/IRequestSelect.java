package com.arsen.peter.project.restaurant.database.request.select;

import com.arsen.peter.project.restaurant.database.item.sort.order.SortingOrder;
import com.arsen.peter.project.restaurant.database.request.Request;

import java.util.List;

public interface IRequestSelect extends Request {

    void addSelectionItem(String key);
    void addWhereItem(String key, Object value);
    void addRangeItem(String key, int min, int max);
    void addLikeItem(String key, Object like);
    void addInRangeItem(String key, List<Object> values);
    void addSortItem(String key, SortingOrder order);
    void setLimit(int limit);
    void setOffset(int offset);

}
