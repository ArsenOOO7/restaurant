package com.arsen.peter.project.restaurant.command;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.defaults.*;
import com.arsen.peter.project.restaurant.command.defaults.admin.CategoryCommand;
import com.arsen.peter.project.restaurant.command.defaults.admin.DishCommand;
import com.arsen.peter.project.restaurant.command.defaults.admin.TableCommand;
import com.arsen.peter.project.restaurant.command.defaults.admin.UserCommand;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CommandMap {

    private Map<String, Command> commandMap = new HashMap<>();

    private static CommandMap instance;
    public static CommandMap getInstance(){
        if(instance == null){
            instance = new CommandMap();
        }
        return instance;
    }


    private CommandMap(){
        registerDefaults();
    }


    public boolean search(String commandValue){

        CommandFormatter commandFormatter = CommandFormatter.format(commandValue);

        String label = commandFormatter.getLabel();
        String[] args = commandFormatter.getArgs();

        if(!commandMap.containsKey(label)){
            return false;
        }

        Command command = commandMap.get(label);

        User user = Application.CURRENT_USER;
        if(command.requireAdmin()){
            if(user != null && user.getStatus() != UserStatus.ADMIN){
                return false;
            }
        }

        if(command.isAuthNeed()){
            if(user == null){
                System.out.println("You need to be authed to do this");
                return true;
            }
        }else{
            if(user != null){
                System.out.println("You have been already authed");
                return true;
            }
        }

        command.execute(args);

        return true;
    }

    public void register(Command command){
        commandMap.put(command.getLabel(), command);
    }

    private void registerDefaults(){
        register(new LoginCommand());
        register(new RegisterCommand());
        register(new SettingsCommand());
        register(new BalanceCommand());
        register(new ExitCommand());
        register(new ExitCommand());

        register(new CategoryCommand());
        register(new DishCommand());
        register(new UserCommand());
        register(new TableCommand());

    }

}
