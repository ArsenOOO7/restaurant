package com.arsen.peter.project.restaurant.service.container.list;

import org.json.JSONArray;

import java.math.BigInteger;
import java.sql.Date;
import java.util.List;

public interface IList extends List<Object> {

    String getString(int key);

    Integer getInt(int key);
    Double getDouble(int key);
    Byte getByte(int key);
    BigInteger getBigInteger(int key);

    Boolean getBoolean(int key);
    Date getDate(int key);
    JSONArray getJSONArray(int key);

}
