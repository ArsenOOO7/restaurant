package com.arsen.peter.project.restaurant.database.request.insert;

import com.arsen.peter.project.restaurant.database.request.Request;

public interface IRequestInsert extends Request {

    void addItem(Object item);

}
