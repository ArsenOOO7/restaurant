package com.arsen.peter.project.restaurant.database.item.where;

import com.arsen.peter.project.restaurant.database.item.RequestItem;
import com.arsen.peter.project.restaurant.database.request.Query;

public class RangeItem implements RequestItem {

    private String key;

    private int min;
    private int max;

    public RangeItem(String key, int min, int max){
        this.key = key;
        this.min = min;
        this.max = max;
    }

    public String getKey() {
        return key;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    @Override
    public String toString() {
        return String.format(Query.RANGE, min, max).replaceAll("\\?", key);
    }
}
