package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.find.UserFind;

public interface IUserRepository extends Repository<User> {

    UserFind getFinder(UserFinders type);

}
