package com.arsen.peter.project.restaurant.repository.table.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class TableFindById extends TableFind{
    @Override
    public Request getRequest() {

        IRequestSelect requestSelect = new RequestSelect();

        requestSelect.setTable(DBTable.TABLES);
        requestSelect.addSelectionItem("*");
        requestSelect.addWhereItem("id", map.getInt("id"));

        return requestSelect;
    }
}
