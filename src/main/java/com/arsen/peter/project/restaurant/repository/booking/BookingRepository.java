package com.arsen.peter.project.restaurant.repository.booking;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.booking.Booking;
import com.arsen.peter.project.restaurant.repository.booking.find.BookingFind;
import com.arsen.peter.project.restaurant.repository.interfaces.IBookingRepository;

public class BookingRepository implements IBookingRepository {
    private static BookingRepository instance;
    public static BookingRepository getInstance(){
        if(instance == null){
            instance = new BookingRepository();
        }
        return instance;
    }

    private BookingRepository(){}

    @Override
    public void add(Booking model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.BOOKING);
        insert.addItem(model.getUser_id());
        insert.addItem(model.getTable_id());
        insert.addItem(model.getDate());

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(Booking model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.BOOKING);
        update.addUpdateItem("user_id", model.getUser_id());
        update.addUpdateItem("table_id", model.getUser_id());
        update.addUpdateItem("date", model.getDate());
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);
    }

    @Override
    public void remove(Booking model) {
        IRequestDelete requestDelete = new RequestDelete();
        requestDelete.setTable(DBTable.BOOKING);
        requestDelete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(requestDelete);
    }

    @Override
    public BookingFind getFinder(BookingFinders type) {
        return type.getConstructor().get();
    }
}
