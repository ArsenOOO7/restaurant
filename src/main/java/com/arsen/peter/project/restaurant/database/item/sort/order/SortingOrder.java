package com.arsen.peter.project.restaurant.database.item.sort.order;

public enum SortingOrder {

    ASC,
    DESC;

}
