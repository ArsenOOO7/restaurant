package com.arsen.peter.project.restaurant.command;

import java.util.Arrays;

public class CommandFormatter {

    private String label;
    private String[] args;

    private CommandFormatter(String command){
        split(command);
    }

    private CommandFormatter(String[] args){
        split(args);
    }

    private void split(String command){

        String[] parts = command.split(" ");

        label = parts[0];
        args = Arrays.copyOfRange(parts, 1, parts.length);

    }


    private void split(String[] args){

        label = args[0];
        this.args = Arrays.copyOfRange(args, 1, args.length);

    }


    public String getLabel() {
        return label;
    }

    public String[] getArgs() {
        return args;
    }


    public static CommandFormatter format(String command){
        return new CommandFormatter(command);
    }

    public static CommandFormatter format(String[] args){
        return new CommandFormatter(args);
    }

}
