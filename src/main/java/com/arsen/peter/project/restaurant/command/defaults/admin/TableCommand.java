package com.arsen.peter.project.restaurant.command.defaults.admin;

import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.repository.table.TableRepository;

public class TableCommand extends Command {


    public TableCommand() {
        super("table");
        addSubCommand(new AddSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return true;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            System.out.println("Usage: table [add]");
        }

        return true;
    }

    private static class AddSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "add";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                System.out.println("Usage: table add seats_number");
                return false;
            }

            String seatsValue = args[0];

            try{

                int seats = Integer.parseInt(seatsValue);
                Table table = new Table();
                table.setSeats(seats);

                TableRepository.getInstance().add(table);
                System.out.println("You've add a new table");

            }catch (NumberFormatException exception){
                System.out.println("Seats_num must be number!");
            }

            return true;
        }
    }

}
