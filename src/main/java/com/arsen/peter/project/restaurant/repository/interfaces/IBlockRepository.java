package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.block.BlockFinders;
import com.arsen.peter.project.restaurant.repository.block.find.BlockFind;

public interface IBlockRepository extends Repository<Block> {

    BlockFind getFinder(BlockFinders type);

}
