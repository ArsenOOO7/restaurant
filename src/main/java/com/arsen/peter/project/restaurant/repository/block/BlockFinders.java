package com.arsen.peter.project.restaurant.repository.block;

import com.arsen.peter.project.restaurant.repository.block.find.BlockFind;
import com.arsen.peter.project.restaurant.repository.block.find.BlockFindByUserId;

import java.util.function.Supplier;

public enum BlockFinders {

    BLOCK_FIND_USER_ID(BlockFindByUserId::new);

    private final Supplier<BlockFind> constructor;

    BlockFinders(Supplier<BlockFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<BlockFind> getConstructor() {
        return constructor;
    }

}
