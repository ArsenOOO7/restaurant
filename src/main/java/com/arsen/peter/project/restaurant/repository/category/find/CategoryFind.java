package com.arsen.peter.project.restaurant.repository.category.find;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.repository.Finder;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

import java.util.ArrayList;
import java.util.List;

public abstract class CategoryFind implements Finder<Category> {

    protected IMap map = new CustomHashMap();

    @Override
    public CategoryFind add(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    @Override
    public CategoryFind add(IMap map) {
        this.map = map;
        return this;
    }

    @Override
    public Category findOne() {
        return findAll().stream().findFirst().orElse(null);
    }

    @Override
    public List<Category> findAll() {
        List<IMap> content = DBManager.getInstance()
                .selectItems(getRequest())
                .getContent();
        List<Category> list = new ArrayList<>();

        for(IMap item: content){
            Category category = new Category();
            category.extract(item);
            list.add(category);
        }

        return list;
    }
}
