package com.arsen.peter.project.restaurant.repository.interfaces;

import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.Repository;
import com.arsen.peter.project.restaurant.repository.dish.DishFinders;
import com.arsen.peter.project.restaurant.repository.dish.find.DishFind;

public interface IDishRepository extends Repository<Dish> {

    DishFind getFinders(DishFinders type);

}
