package com.arsen.peter.project.restaurant.repository.user.find;

import com.arsen.peter.project.restaurant.database.request.Request;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.table.DBTable;

public class UserFindByLogin extends UserFind{
    @Override
    public Request getRequest() {
        IRequestSelect select = new RequestSelect();

        select.setTable(DBTable.USERS_VIEW);
        select.addSelectionItem("*");
        select.addWhereItem("login", map.getString("login"));

        return select;
    }
}
