package com.arsen.peter.project.restaurant.repository.bookingDish;

import com.arsen.peter.project.restaurant.repository.bookingDish.find.BookingDishFind;
import com.arsen.peter.project.restaurant.repository.bookingDish.find.BookingDishFindByUserId;

import java.util.function.Supplier;

public enum BookingDishFinders {

    BOOKING_DISH_FIND_USER_ID(BookingDishFindByUserId::new);

    private final Supplier<BookingDishFind> constructor;

    BookingDishFinders(Supplier<BookingDishFind> findSupplier){
        this.constructor = findSupplier;
    }

    public Supplier<BookingDishFind> getConstructor() {
        return constructor;
    }

}
