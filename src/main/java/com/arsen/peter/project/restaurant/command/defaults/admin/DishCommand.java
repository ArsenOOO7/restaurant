package com.arsen.peter.project.restaurant.command.defaults.admin;

import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandFormatter;
import com.arsen.peter.project.restaurant.command.subcommand.SubCommand;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.category.CategoryFinders;
import com.arsen.peter.project.restaurant.repository.category.CategoryRepository;
import com.arsen.peter.project.restaurant.repository.dish.DishFinders;
import com.arsen.peter.project.restaurant.repository.dish.DishRepository;

import java.util.List;

public class DishCommand extends Command {

    public DishCommand() {
        super("dish");
        addSubCommand(new AddSubCommand());
        addSubCommand(new ListSubCommand());
        addSubCommand(new RemoveSubCommand());
    }

    @Override
    public boolean requireAdmin() {
        return true;
    }

    @Override
    public boolean isAuthNeed() {
        return true;
    }

    @Override
    public boolean execute(String[] args) {

        if(args.length < 1){
            System.out.println("Usage: dish [add/list/remove]");
            return false;
        }

        String sub = args[0];
        SubCommand subCommand = getSubCommand(sub);

        if(subCommand == null){
            System.out.println("Usage: dish [add/list/remove]");
            return false;
        }

        subCommand.executeSub(CommandFormatter.format(args).getArgs());

        return true;
    }


    private static class AddSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "add";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 3){
                System.out.println("Usage: dish add [category_id] [identifier] [price]");
                return false;
            }

            String valueCategoryId = args[0];
            String identifier = args[1];
            String valuePrice = args[2];

            try{

                int categoryId = Integer.parseInt(valueCategoryId);
                int price = Integer.parseInt(valuePrice);

                Category foundCategory = CategoryRepository.getInstance().getFinders(CategoryFinders.CATEGORY_FIND_ID)
                        .add("id", categoryId)
                        .findOne();

                if(foundCategory == null){
                    System.out.println("There is no such category");
                    return false;
                }

                price = Math.abs(price);

                if(price == 0){
                    System.out.println("Price must be > 0");
                    return false;
                }

                //I should add the check of existing such category with this identifier :( ...

                Dish dish = new Dish();
                dish.setCategory_id(categoryId);
                dish.setPrice(price);
                dish.setIdentifier(identifier);

                DishRepository.getInstance().add(dish);
                System.out.println("You've successfully added a new dish!");

            }catch (NumberFormatException formatException){
                System.out.println("Category id | price must be number!");
            }

            return true;
        }
    }

    private static class ListSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "list";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                System.out.println("Usage: dish list [category_id]");
                return false;
            }

            String valueCategoryId = args[0];
            try{

                int categoryId = Integer.parseInt(valueCategoryId);
                List<Dish> dishList = DishRepository.getInstance().getFinders(DishFinders.DISH_FIND_CATEGORY)
                        .add("category_id", categoryId)
                        .findAll();

                if(dishList.size() == 0){
                    System.out.println("Empty! Check the category id");
                    return false;
                }

                StringBuilder builder = new StringBuilder();
                builder.append("ID\t").append("NAME\t").append("PRICE\n");
                dishList.forEach(dish ->
                        builder.append(dish.getId()).append("\t")
                        .append(dish.getIdentifier()).append("\t")
                        .append(dish.getPrice()).append("\n"));

                System.out.println(builder);

            }catch (NumberFormatException exception){
                System.out.println("Category id must be number!");
            }

            return true;
        }
    }

    private static class RemoveSubCommand implements SubCommand{

        @Override
        public String getLabel() {
            return "remove";
        }

        @Override
        public boolean executeSub(String[] args) {

            if(args.length != 1){
                System.out.println("Usage: dish remove [id]");
                return false;
            }

            String valueId = args[0];

            try{

                int id = Integer.parseInt(valueId);
                Dish dish = new Dish();
                dish.setId(id);

                DishRepository.getInstance().remove(dish);
                System.out.println("You've removed dish");

            }catch (NumberFormatException formatException){
                System.out.println("Id must be number!");
            }

            return true;
        }
    }

}
