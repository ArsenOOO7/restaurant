package com.arsen.peter.project.restaurant.repository.bookingDish;

import com.arsen.peter.project.restaurant.database.DBManager;
import com.arsen.peter.project.restaurant.database.request.delete.IRequestDelete;
import com.arsen.peter.project.restaurant.database.request.delete.RequestDelete;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.repository.bookingDish.find.BookingDishFind;
import com.arsen.peter.project.restaurant.repository.interfaces.IBookingDishRepository;
import org.json.JSONArray;

public class BookingDishRepository implements IBookingDishRepository {
    private static BookingDishRepository instance;
    public static BookingDishRepository getInstance(){
        if(instance == null){
            instance = new BookingDishRepository();
        }
        return instance;
    }

    private BookingDishRepository(){}
    @Override
    public void add(BookingDish model) {
        IRequestInsert insert = new RequestInsert();
        insert.setTable(DBTable.BOOKING_DISHES);
        insert.addItem(model.getUser_id());
        insert.addItem(model.getBooking_id());

        JSONArray dishes = new JSONArray(model.getDishes().stream().map(Dish::getId).toList());
        insert.addItem(dishes);

        int id = DBManager.getInstance().insertItem(insert);
        model.setId(id);
    }

    @Override
    public void update(BookingDish model) {
        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.BOOKING_DISHES);
        update.addUpdateItem("user_id", model.getUser_id());
        update.addUpdateItem("booking_id", model.getBooking_id());
        update.addUpdateItem("dishes_id", new JSONArray(model.getDishes().stream().map(Dish::getId).toList()));
        update.addWhereItem("id", model.getId());

        DBManager.getInstance().updateItem(update);

    }

    @Override
    public void remove(BookingDish model) {
        IRequestDelete requestDelete = new RequestDelete();
        requestDelete.setTable(DBTable.BOOKING_DISHES);
        requestDelete.addWhereItem("id", model.getId());

        DBManager.getInstance().removeItems(requestDelete);

    }

    @Override
    public BookingDishFind getFinder(BookingDishFinders type) {
        return type.getConstructor().get();
    }
}
