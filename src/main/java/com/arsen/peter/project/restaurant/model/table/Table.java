package com.arsen.peter.project.restaurant.model.table;

import com.arsen.peter.project.restaurant.model.Model;
import com.arsen.peter.project.restaurant.service.container.map.IMap;

public class Table implements Model {

    private int id;
    private int seats;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void extract(IMap result) {
        setId(result.getInt("id"));
        setSeats(result.getInt("seats"));
    }
}
