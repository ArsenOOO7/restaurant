package com.arsen.peter.project.restaurant.database.item.select;

import com.arsen.peter.project.restaurant.database.item.RequestItem;

public class SelectItem implements RequestItem {

    private String key;

    public SelectItem(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        return key;
    }
}
