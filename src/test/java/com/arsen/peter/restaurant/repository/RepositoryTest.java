package com.arsen.peter.restaurant.repository;

import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.model.booking.Booking;
import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.block.BlockFinders;
import com.arsen.peter.project.restaurant.repository.block.BlockRepository;
import com.arsen.peter.project.restaurant.repository.booking.BookingFinders;
import com.arsen.peter.project.restaurant.repository.booking.BookingRepository;
import com.arsen.peter.project.restaurant.repository.bookingDish.BookingDishRepository;
import com.arsen.peter.project.restaurant.repository.category.CategoryFinders;
import com.arsen.peter.project.restaurant.repository.category.CategoryRepository;
import com.arsen.peter.project.restaurant.repository.category.find.CategoryFind;
import com.arsen.peter.project.restaurant.repository.dish.DishFinders;
import com.arsen.peter.project.restaurant.repository.dish.DishRepository;
import com.arsen.peter.project.restaurant.repository.dish.find.DishFind;
import com.arsen.peter.project.restaurant.repository.interfaces.*;
import com.arsen.peter.project.restaurant.repository.table.TableFinders;
import com.arsen.peter.project.restaurant.repository.table.TableRepository;
import com.arsen.peter.project.restaurant.repository.user.UserFinders;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;
import com.arsen.peter.restaurant.database.DBRequestTest;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RepositoryTest extends DBRequestTest {

    @Test
    public void testAddingUser(){

        //Test adding new users to DB
        User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        IUserRepository userRepository = UserRepository.getInstance();
        userRepository.add(user);

        assertEquals(1, user.getId());

        User user2 = User.createUser("Peter", "Slobodian", "peterslobodian@gmail.com",
                Date.valueOf("2004-02-19"), 10000, "Peter", "hispassword", UserStatus.USER);

        userRepository.add(user2);

        assertEquals(2, user2.getId());

    }

    @Test
    public void testUpdateUser(){

        User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                                        Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        IUserRepository userRepository = UserRepository.getInstance();
        userRepository.add(user);

        user.setStatus(UserStatus.ADMIN);
        userRepository.update(user);

        User foundUser = userRepository.getFinder(UserFinders.USER_FIND_ID)
                .add("id", user.getId())
                .findOne();

        assertEquals(UserStatus.ADMIN, foundUser.getStatus());

    }


    @Test
    public void testExistsUser(){

        IUserRepository userRepository = UserRepository.getInstance();
        User foundUser = userRepository.getFinder(UserFinders.USER_FIND_ID)
                .add("id", 1)
                .findOne();

        assertNull(foundUser);

    }


    @Test
    public void testBanUser(){

        User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        IUserRepository userRepository = UserRepository.getInstance();
        userRepository.add(user);

        Block block = new Block();
        block.setUser_id(user.getId());
        block.setReason("test reason");
        block.setEnd_time(60 * 60);
        IBlockRepository blockRepository = BlockRepository.getInstance();
        blockRepository.add(block);

        User bannedUser = userRepository.getFinder(UserFinders.USER_FIND_ID)
                .add("id", user.getId())
                .findOne();

        assertTrue(bannedUser.isBlocked());
        assertEquals("test reason", bannedUser.getBlock().getReason());

        userRepository.remove(user);
        Block foundBlock = BlockRepository.getInstance().getFinder(BlockFinders.BLOCK_FIND_USER_ID)
                .add("user_id", user.getId())
                .findOne();

        assertNull(foundBlock);


    }


    @Test
    public void testClearUser(){

        User user = User.createUser("", "", "",
                new Date(0), 0, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        UserRepository.getInstance().add(user);
        assertEquals(1, user.getId());

    }


    @Test
    public void testAuth(){

        IUserRepository userRepository = UserRepository.getInstance();
        User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        userRepository.add(user);
        assertEquals(1, user.getId());

        //test for signing in via non-existing email;

        String email = "123@gmail.com";
        User foundUserEmail = userRepository.getFinder(UserFinders.USER_FIND_EMAIL)
                .add("email", email).findOne();

        assertNull(foundUserEmail);

        String login = "ArsenOOO7";
        String incorrectPass = "11";
        String correctPass = "mysecretpassword";


        //Test user auth by login
        User foundUserLogin = userRepository.getFinder(UserFinders.USER_FIND_LOGIN)
                .add("login", login).findOne();

        assertNotNull(foundUserLogin);

        assertNotEquals(foundUserLogin.getPassword(), incorrectPass);
        assertEquals(foundUserLogin.getPassword(), correctPass);

    }


    @Test
    public void testCategory(){

        ICategoryRepository categoryRepository = CategoryRepository.getInstance();
        List<Category> categories = getCategories();

        categories.forEach(categoryRepository::add);

        CategoryFind finderAll = categoryRepository.getFinders(CategoryFinders.CATEGORY_FIND_ALL);

        assertThrows(UnsupportedOperationException.class, finderAll::findOne);

        List<Category> findAll = finderAll.findAll();

        assertEquals(categories.size(), findAll.size());

    }

    @Test
    public void testBooking(){

        User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);

        List<Category> categories = getCategories();
        List<Dish> dishes = getDishes();
        List<Table> tables = getTables();

        Booking booking = new Booking();
        BookingDish bookingDish = new BookingDish();

        IUserRepository userRepository = UserRepository.getInstance();
        ICategoryRepository categoryRepository = CategoryRepository.getInstance();
        IDishRepository dishRepository = DishRepository.getInstance();
        ITableRepository tableRepository = TableRepository.getInstance();
        IBookingRepository bookingRepository = BookingRepository.getInstance();

        userRepository.add(user);
        categories.forEach(categoryRepository::add);
        dishes.forEach(dishRepository::add);
        tables.forEach(tableRepository::add);

        assertEquals(1, user.getId());


        //Start booking
        booking.setUser_id(user.getId());


        //Our user wants to book 2-seat table
        int findSeats = 2;
        Table table = tableRepository.getFinders(TableFinders.TABLE_FIND_SEATS)
                .add("seats", findSeats)
                .findOne();

        assertNotNull(table);
        assertEquals(1, table.getId());

        booking.setTable_id(table.getId()); //1


        //Now he selects date
        Timestamp timestamp = Timestamp.valueOf("2022-07-20 20:00:00");
        booking.setDate(timestamp);

        //But before we should make sure that the table is available
        Booking checkIfExists = bookingRepository.getFinder(BookingFinders.BOOKING_FIND_TABLE_ID)
                .add("table_id", table.getId())
                .findOne();

        assertNull(checkIfExists); //true

        //Now he decides to pre-order some dishes
        //He wants something to drink, to eat and one dessert

        String beverage = "beer";
        String mainDish = "pizza";
        String dessert = "ice cream";

        DishFind finder = dishRepository.getFinders(DishFinders.DISH_FIND_IDENTIFIER);
        Dish beer = finder.add("identifier", beverage).findOne();
        Dish pizza = finder.add("identifier", mainDish).findOne();
        Dish iceCream = finder.add("identifier", dessert).findOne();

        assertNotNull(beer);
        assertNotNull(pizza);
        assertNotNull(iceCream);

        bookingDish.addDish(beer);
        bookingDish.addDish(pizza);
        bookingDish.addDish(iceCream);

        bookingDish.setUser_id(user.getId());

        //Now we can make full order
        bookingRepository.add(booking);
        assertEquals(1, booking.getId());

        bookingDish.setBooking_id(booking.getId());
        BookingDishRepository.getInstance().add(bookingDish);
        assertEquals(1, bookingDish.getId());

        //Let's check this ...
        Booking fullOrder = bookingRepository.getFinder(BookingFinders.BOOKING_FIND_USER_ID)
                .add("user_id", user.getId())
                .findOne();

        assertNotNull(fullOrder);
        assertEquals(1, fullOrder.getId());
        assertEquals(1, fullOrder.getUser_id());
        assertEquals(1, fullOrder.getTable_id());

        assertTrue(fullOrder.hasBookedAnyDish());

        BookingDish bookingDishOrder = fullOrder.getBookingDish();
        assertEquals(1, bookingDishOrder.getId());
        assertEquals(3, bookingDishOrder.getDishes().size());

        List<String> expectedDishes = List.of(beverage, dessert, mainDish);
        List<String> actualDishes = bookingDishOrder.getDishes()
                .stream()
                .map(Dish::getIdentifier)
                .sorted()
                .toList();
        
        assertEquals(expectedDishes, actualDishes);

    }


    private static final String[] categories = {"beverage", "dessert", "soup", "main dish", "salad"};
    private static final String[][] dishes = {
            {"beer", "orange juice", "cola", "pepsi", "sprite"},
            {"ice cream", "apple pie", "cheesecake", "blackberry"},
            {"borsch", "mushroom soup", "tomato soup"},
            {"spaghetti", "potato", "pork roast", "pizza", "bacon"},
            {"olivye", "caesar", "summer asian slaw", "easy pasta", "rainbow"}
    };

    private static final Integer[] tables = {2, 2, 2, 2, 3, 3, 5, 6, 7};

    private static List<Category> getCategories(){
        List<Category> categoriesList = new ArrayList<>();

        for (String categoryName : categories) {
            Category category = new Category();
            category.setIdentifier(categoryName);
            categoriesList.add(category);
        }

        return categoriesList;
    }

    private static List<Dish> getDishes(){

        List<Dish> dishesList = new ArrayList<>();
        Random random = new Random();

        for(int i = 0; i < dishes.length; ++i){
            for(String dishName: dishes[i]){
                Dish dish = new Dish();
                dish.setCategory_id(i + 1);
                dish.setIdentifier(dishName);
                dish.setPrice(random.nextInt(500) + 1);

                dishesList.add(dish);
            }
        }

        return dishesList;
    }


    private static List<Table> getTables(){

        List<Table> tablesList = new ArrayList<>();

        for(int seats: tables){
            Table table = new Table();
            table.setSeats(seats);

            tablesList.add(table);
        }

        return tablesList;
    }

}
