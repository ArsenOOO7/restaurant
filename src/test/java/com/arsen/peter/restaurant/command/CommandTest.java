package com.arsen.peter.restaurant.command;

import com.arsen.peter.project.restaurant.Application;
import com.arsen.peter.project.restaurant.command.Command;
import com.arsen.peter.project.restaurant.command.CommandMap;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.repository.category.CategoryFinders;
import com.arsen.peter.project.restaurant.repository.category.CategoryRepository;
import com.arsen.peter.project.restaurant.repository.user.UserRepository;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;
import com.arsen.peter.restaurant.database.DBRequestTest;
import com.arsen.peter.restaurant.repository.RepositoryTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Date;
import java.util.stream.Stream;

public class CommandTest extends DBRequestTest {

    private static ByteArrayOutputStream out;
    private static PrintStream printStream;

    private static final PrintStream DEFAULT_OUT = System.out;

    private static User emptyUser = User.createUser("", "", "",
                        new Date(0), 0, "SomeNick", "mysecretpassword", UserStatus.USER);
    private static User user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
            Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);
    

    @BeforeEach
    public void beforeEach(){
        CommandMap.getInstance();

        out = new ByteArrayOutputStream();
        printStream = new PrintStream(out);

        System.setOut(printStream);
    }


    @AfterEach
    public void afterEach(){
        System.setOut(DEFAULT_OUT);
        Application.CURRENT_USER = null;

        emptyUser = User.createUser("", "", "",
                new Date(0), 0, "SomeNick", "mysecretpassword", UserStatus.USER);

        user = User.createUser("Arsen", "Sydoryk", "arsen.sydoryk@gmail.com",
                Date.valueOf("2004-03-15"), 10000, "ArsenOOO7", "mysecretpassword", UserStatus.USER);
    }

    @ParameterizedTest
    @MethodSource("provideLoginCommandTest")
    public void loginTest(String command, String expected){

        UserRepository.getInstance().add(user);

        assertTrue(CommandMap.getInstance().search(command));

        printStream.flush();
        assertEquals(expected, out.toString().trim());

    }


    @ParameterizedTest
    @MethodSource("provideRegisterCommandTest")
    public void registerTest(String command, String expected){

        UserRepository.getInstance().add(user);

        assertTrue(CommandMap.getInstance().search(command));

        printStream.flush();
        assertEquals(expected, out.toString().trim());

    }


    @Test
    public void settingsTest(){

        UserRepository.getInstance().add(user);
        UserRepository.getInstance().add(emptyUser);

        String command = "settings";

        //Test for empty user
        Application.CURRENT_USER = emptyUser;

        getEmptyUserInput().forEach(input ->  assertTrue(CommandMap.getInstance()
                .search(String.join(" ", command, "set", input))));

        assertEquals("Name1", emptyUser.getName());
        assertEquals("Surname1", emptyUser.getSurname());
        assertEquals("myemail@mail.com", emptyUser.getEmail());
        assertEquals(Date.valueOf("2004-03-15"), emptyUser.getBirth_date());

        //Test for show
        out.reset();
        CommandMap.getInstance().search(command + " show");
        printStream.flush();

        assertEquals(getEmptyUserShow().trim(), out.toString().trim());

        //Test for existing email
        out.reset();
        assertTrue(CommandMap.getInstance().search(command + " set email arsen.sydoryk@gmail.com"));
        printStream.flush();

        assertEquals("Email already exists", out.toString().trim());

    }

    @ParameterizedTest
    @MethodSource("provideBalanceCommandTest")
    public void balanceTest(String command, String expected){

        UserRepository.getInstance().add(user);
        Application.CURRENT_USER = user;

        assertTrue(CommandMap.getInstance().search(command));

        printStream.flush();
        assertEquals(expected, out.toString().trim());

    }


    @Test
    public void categoryTest(){

        Application.CURRENT_USER = user;
        String command = "category";

        assertFalse(CommandMap.getInstance().search(command));

        user.setStatus(UserStatus.ADMIN);

        assertTrue(CommandMap.getInstance().search(command));

        printStream.flush();
        assertEquals("Usage: category [add/list/remove]", out.toString().trim());

        out.reset();
        for (String category : categories) {
            CommandMap.getInstance().search(command + " add " + category);

            printStream.flush();
            assertEquals("You've added new category", out.toString().trim());
            out.reset();
        }

        CommandMap.getInstance().search(command + " add " + categories[0]);

        printStream.flush();
        assertEquals("There is a category with same name!", out.toString().trim());
        out.reset();

        String expectedList = getListCategoriesView();
        CommandMap.getInstance().search(command + " list");

        printStream.flush();
        assertEquals(expectedList.trim(), out.toString().trim());
        out.reset();

        int removeId = 1;
        int fakeId = categories.length + 1;

        CommandMap.getInstance().search(command + " remove " + fakeId);

        printStream.flush();
        assertEquals("Non-existing category", out.toString().trim());
        out.reset();

        CommandMap.getInstance().search(command + " remove abc");

        printStream.flush();
        assertEquals("Id must be number!", out.toString().trim());
        out.reset();

        CommandMap.getInstance().search(command + " remove " + removeId);

        printStream.flush();
        assertEquals("You successfully removed category!", out.toString().trim());
        out.reset();

        Category category = CategoryRepository.getInstance().getFinders(CategoryFinders.CATEGORY_FIND_ID)
                .add("id", removeId)
                .findOne();

        assertNull(category);

    }


    private static Stream<Arguments> provideLoginCommandTest(){
        return Stream.of(

                Arguments.of("login", "Usage: login [login] [password]"),
                Arguments.of("login Arsen", "Usage: login [login] [password]"),
                Arguments.of("login ArsenOO arsen", "Incorrect login!"),
                Arguments.of("login ArsenOOO7 mypass", "Incorrect password"),
                Arguments.of("login ArsenOOO7 mypass", "Incorrect password"),
                Arguments.of("login ArsenOOO7 mysecretpassword", "You have successfully signed in"),
                Arguments.of("login arsen.sydoryk@gmail.com mysecretpassword", "You have successfully signed in")

        );
    }

    private static Stream<Arguments> provideRegisterCommandTest(){
        return Stream.of(

                Arguments.of("register", "Usage: register [login] [password] [password repeat]"),
                Arguments.of("register ArsenOOO7", "Usage: register [login] [password] [password repeat]"),
                Arguments.of("register ArsenOOO7 pass1 pass2", "User with this login already exists"),
                Arguments.of("register someLogin pass1 pass2", "You have repeated the password incorrectly"),
                Arguments.of("register someLogin pass1 pass1", "You have successfully registered. Now you can sign in (login cmd)")

        );
    }

    private static Stream<Arguments> provideBalanceCommandTest(){
        return Stream.of(

                Arguments.of("balance", "Your balance: " + user.getBalance()),
                Arguments.of("balance top", "Usage: balance [topup]"),
                Arguments.of("balance topup", "Usage: balance topup [money]"),
                Arguments.of("balance topup abc", "Money must be number!"),
                Arguments.of("balance topup 0", "Amount must be > 0!"),
                Arguments.of("balance topup 1000", "You've successfully topped up your balance!")

        );
    }


    private static Stream<String> getEmptyUserInput(){
        return Stream.of("name Name1", "surname Surname1", "email myemail@mail.com", "birth 2004-03-15");
    }


    private static String getEmptyUserShow(){
        StringBuilder builder = new StringBuilder();
        getEmptyUserInput().forEach(input -> {

            String[] params = input.split(" ");
            String value = "Your " + params[0] + ": " + params[1] + "\n";

            builder.append(value);

        });

        return builder.toString();
    }

    private static final String[] categories = {"beverage", "dessert", "soup", "main:dish", "salad"};

    private static String getListCategoriesView(){

        StringBuilder builder = new StringBuilder();
        builder.append("ID\tNAME");
        for (int i = 0; i < categories.length; ++i) {
            builder.append(i + 1).append("\t")
                    .append(categories[i].replace(":", " "))
                    .append("\n");
        }

        return new String(builder);
    }

}
