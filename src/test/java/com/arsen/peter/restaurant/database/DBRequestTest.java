package com.arsen.peter.restaurant.database;

import com.arsen.peter.project.restaurant.database.DBManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBRequestTest {

    private static final String APP_PROPS_FILE = "app.properties";
    private static final String SQL_FILE = "db/restaurant.sql";

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/restaurant_test?createDatabaseIfNotExist=true&user=root&password=";

    private static final String APP_CON_CONTENT = "database.connection=" + CONNECTION_URL;

    private static final String CREATE_DATABASE = "CREATE DATABASE IF NOT EXISTS restaurant_test";
    private static final String USE_DATABASE = "USE restaurant_test;";
    private static final String DROP_DATABASE = "DROP DATABASE restaurant_test";

    private static String definedAddress;
    private static String definedDriver;
    private static URL resource;

    private static List<String> queriesSQL = new ArrayList<>();
//    private static String queriesSQL;

    private static Connection connection;

    @BeforeAll
    public static void globalSetUp() throws SQLException, IOException, URISyntaxException {

        connection = DriverManager.getConnection(CONNECTION_URL);
        readProperties();

    }

    private static void readProperties() throws IOException, URISyntaxException{

        resource = DBRequestTest.class.getClassLoader().getResource(APP_PROPS_FILE);
        Path path = Path.of(resource.toURI());

        List<String> lines = Files.readAllLines(path);

        definedAddress = lines.get(0);
        definedDriver = lines.get(1);

        String queriesAll = String.join("", Files.readAllLines(Path.of(SQL_FILE)));
        queriesSQL = List.of(queriesAll.split(";"));


        Files.write(path, List.of(APP_CON_CONTENT, definedDriver));

    }


    @AfterAll
    public static void globalTearDown() throws URISyntaxException, IOException {

        Path path = Path.of(resource.toURI());
        Files.write(path, List.of(definedAddress, definedDriver));

    }



    @BeforeEach
    public void setUp() throws SQLException {

        connection.createStatement().executeUpdate(CREATE_DATABASE);
        connection.createStatement().executeUpdate(USE_DATABASE);

        for(String query: queriesSQL) {
            connection.createStatement().executeUpdate(query);
        }
        DBManager.getInstance();

    }


    @AfterEach
    public void tearDown() throws SQLException {

        connection.createStatement().executeUpdate(DROP_DATABASE);

    }
}
