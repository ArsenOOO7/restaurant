package com.arsen.peter.restaurant.database;

import com.arsen.peter.project.restaurant.database.item.sort.order.SortingOrder;
import com.arsen.peter.project.restaurant.database.request.insert.IRequestInsert;
import com.arsen.peter.project.restaurant.database.request.insert.RequestInsert;
import com.arsen.peter.project.restaurant.database.request.select.IRequestSelect;
import com.arsen.peter.project.restaurant.database.request.select.RequestSelect;
import com.arsen.peter.project.restaurant.database.request.update.IRequestUpdate;
import com.arsen.peter.project.restaurant.database.request.update.RequestUpdate;
import com.arsen.peter.project.restaurant.database.table.DBTable;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DBConstructorTest {

    @Test
    public void testInsertion(){

        IRequestInsert requestInsert = new RequestInsert();
        requestInsert.setTable(DBTable.TEST);
        requestInsert.addItem("Arsen"); //name
        requestInsert.addItem("Sydoryk"); //surname
        requestInsert.addItem(18); //age
        requestInsert.addItem(false); //has driving license
        requestInsert.addItem(true); //wants to get it
        requestInsert.addItem("Soon"); //when he wants to get it

        String expected = "INSERT INTO test VALUES(DEFAULT, 'Arsen', 'Sydoryk', '18', 'false', 'true', 'Soon')";
        String actual = requestInsert.build();
        assertEquals(removeSpace(expected), removeSpace(actual));

    }


    @Test
    public void testSelection(){

        //Select all from table test
        IRequestSelect selectAll = new RequestSelect();
        selectAll.setTable(DBTable.TEST);
        selectAll.addSelectionItem("*");

        String expected = "SELECT * FROM test";
        assertEquals(expected, selectAll.build());

        // Simple test for a few selection items
        IRequestSelect selectTest1 = new RequestSelect();
        selectTest1.setTable(DBTable.TEST);
        selectTest1.addSelectionItem("name");
        selectTest1.addSelectionItem("surname");
        selectTest1.addSelectionItem("age");

        selectTest1.addWhereItem("id", 1);
        expected = "SELECT name, surname, age FROM test WHERE id=1";
        assertEquals(expected, selectTest1.build());

        //Test for all selection items
        IRequestSelect selectTest2 = new RequestSelect();
        selectTest2.setTable(DBTable.TEST);
        selectTest2.addSelectionItem("*");

        selectTest2.addWhereItem("id", 1);
        expected = "SELECT * FROM test WHERE id=1";
        assertEquals(expected, selectTest2.build());

        //Test for one selection item
        IRequestSelect selectTest3 = new RequestSelect();
        selectTest3.setTable(DBTable.TEST);
        selectTest3.addSelectionItem("name");

        selectTest3.addWhereItem("age", 20);
        expected = "SELECT name FROM test WHERE age='20'";
        assertEquals(expected, selectTest3.build());

        //Request for getting list of items for ranged criteria
        IRequestSelect selectTest4 = new RequestSelect();
        selectTest4.setTable(DBTable.TEST);
        selectTest4.addSelectionItem("name");

        selectTest4.addRangeItem("age", 10, 20);
        expected = "SELECT name FROM test WHERE age >= 10 AND age <= 20";
        assertEquals(expected, selectTest4.build());

        //Get list of items via LIKE and range
        IRequestSelect selectTest5 = new RequestSelect();
        selectTest5.setTable(DBTable.TEST);
        selectTest5.addSelectionItem("*");

        selectTest5.addLikeItem("name", "A");
        selectTest5.addRangeItem("age", 10, 20);
        expected = "SELECT * FROM test WHERE name LIKE 'A%' AND age >= 10 AND age <= 20";
        assertEquals(expected, selectTest5.build());

    }


    @Test
    public void testSortedSelection(){

        //Test selection without criteria
        IRequestSelect selectTest1 = new RequestSelect();
        selectTest1.setTable(DBTable.TEST);
        selectTest1.addSelectionItem("*");

        selectTest1.addSortItem("age", SortingOrder.ASC);
        String expected = "SELECT * FROM test ORDER BY age ASC";
        assertEquals(expected, selectTest1.build());

        //Test sorted selection with criteria and sort
        IRequestSelect selectTest2 = new RequestSelect();
        selectTest2.setTable(DBTable.TEST);
        selectTest2.addSelectionItem("*");

        selectTest2.addLikeItem("name", "A");
        selectTest2.addWhereItem("license", true);

        selectTest2.addSortItem("age", SortingOrder.ASC);
        expected = "SELECT * FROM test WHERE name LIKE 'A%' AND license='true' ORDER BY age ASC";
        assertEquals(expected, selectTest2.build());

    }


    @Test
    public void testFinalSelection(){
        IRequestSelect finalTest = new RequestSelect();
        finalTest.setTable(DBTable.TEST);

        finalTest.addSelectionItem("name");
        finalTest.addSelectionItem("surname");
        finalTest.addSelectionItem("age");

        finalTest.addWhereItem("license", true);
        finalTest.addLikeItem("name", "A");
        finalTest.addRangeItem("age", 18, 20);

        finalTest.addSortItem("age", SortingOrder.ASC);

        String expected = "SELECT name, surname, age FROM test WHERE license='true' AND name LIKE 'A%' AND age >= 18 AND age <= 20 ORDER BY age ASC";
        assertEquals(expected, finalTest.build());

    }


    @Test
    public void testUpdate(){

        IRequestUpdate update = new RequestUpdate();
        update.setTable(DBTable.TEST);

        update.addUpdateItem("name", "NewName");
        update.addUpdateItem("surname", "NewSurname");
        update.addUpdateItem("age", "20");

        update.addWhereItem("id", 1);

        String expected = "UPDATE test SET name='NewName', surname='NewSurname', age='20' WHERE id=1";
        assertEquals(expected, update.build());

    }

    public String removeSpace(String request){

        return request
                .replace(", ", ",")
                .replace(" >= ", ">=")
                .replace(" >=", ">=")
                .replace(">= ", ">=")
                .replace(" <= ", "<=")
                .replace(" <=", "<=")
                .replace("<= ", "<=");

    }

}
