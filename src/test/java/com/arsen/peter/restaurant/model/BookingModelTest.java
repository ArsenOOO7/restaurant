package com.arsen.peter.restaurant.model;

import com.arsen.peter.project.restaurant.model.booking.Booking;
import com.arsen.peter.project.restaurant.model.bookingDish.BookingDish;
import com.arsen.peter.project.restaurant.model.category.Category;
import com.arsen.peter.project.restaurant.model.dish.Dish;
import com.arsen.peter.project.restaurant.model.table.Table;
import com.arsen.peter.project.restaurant.model.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;


public class BookingModelTest {

    @Test
    public void testNormal(){

        User user = new User();
        Table table = new Table();
        Booking booking = new Booking();

        user.setId(1);

        table.setId(1);
        table.setSeats(5);

        booking.setId(1);
        booking.setUser_id(user.getId());
        booking.setTable_id(table.getId());

        Assertions.assertEquals(1, user.getId());

        Assertions.assertEquals(1, table.getId());
        Assertions.assertEquals(5, table.getSeats());

        Assertions.assertEquals(1, booking.getId());
        Assertions.assertEquals(user.getId(), booking.getUser_id());
        Assertions.assertEquals(table.getId(), booking.getTable_id());


    }

    @Test
    public void testFullBooking(){
        Category category = new Category();

        User user = new User();
        Table table = new Table();
        Booking booking = new Booking();
        BookingDish bookingDish = new BookingDish();

        user.setId(1);
        table.setId(1);
        booking.setId(1);
        booking.setBookingDish(bookingDish);
        bookingDish.setId(1);
        category.setId(1);
        category.setIdentifier("beverage");

        String arr[] = {"beer", "orange juice", "sparkling water", "cola", "pepsi", "zhyvchyk"};
        Random  random  = new Random();

        for (int i = 1; i <= arr.length; i++) {
            Dish dish = new Dish();
            dish.setId(i);
            dish.setIdentifier(arr[i-1]);
            dish.setPrice(random.nextInt(100));
            dish.setCategory_id(1);
            bookingDish.addDish(dish);
        }


        Assertions.assertEquals(6, bookingDish.getDishes().size());
        Assertions.assertTrue(booking.hasBookedAnyDish());


    }


}
