package com.arsen.peter.restaurant.model;

import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;

public class UserModelTest {

    @Test
    public void testNormal(){

        User user = new User();
        user.setId(1);
        user.setName("Name1");
        user.setSurname("surname");
        user.setLogin("12345");
        user.setPassword("qwerty");
        user.setEmail("user@gmail.com");
        user.setBirth_date(Date.valueOf("2000-12-4"));
        user.setBalance(123212411);
        user.setStatus(UserStatus.USER);

        Assertions.assertEquals(1, user.getId());
        Assertions.assertEquals("Name1", user.getName());
        Assertions.assertEquals("surname", user.getSurname());
        Assertions.assertEquals("12345", user.getLogin());
        Assertions.assertEquals("qwerty", user.getPassword());
        Assertions.assertEquals("user@gmail.com", user.getEmail());
        Assertions.assertEquals("2000-12-04", user.getBirth_date().toString());
        Assertions.assertEquals(123212411, user.getBalance());
        Assertions.assertEquals(UserStatus.USER, user.getStatus());

    }

    @Test
    public void testExtract(){

        IMap map = new CustomHashMap();
        map.put("id", 1);
        map.put("name", "Name1");
        map.put("surname", "Surname1");
        map.put("login", "12345");
        map.put("password", "qwerty");
        map.put("email", "user@gmail.com");
        map.put("birth_date", Date.valueOf("2000-12-04"));
        map.put("balance", 1000000);
        map.put("status", 0);
        map.put("blocked", 0);


        User user = new User();
        user.extract(map);
        Assertions.assertEquals(1, user.getId());
        Assertions.assertEquals("Name1", user.getName());
        Assertions.assertEquals("Surname1", user.getSurname());
        Assertions.assertEquals("12345", user.getLogin());
        Assertions.assertEquals("qwerty", user.getPassword());
        Assertions.assertEquals("user@gmail.com", user.getEmail());
        Assertions.assertEquals("2000-12-04", user.getBirth_date().toString());
        Assertions.assertEquals(1000000, user.getBalance());
        Assertions.assertEquals(UserStatus.USER, user.getStatus());
        Assertions.assertFalse(user.isBlocked());


    }


}
