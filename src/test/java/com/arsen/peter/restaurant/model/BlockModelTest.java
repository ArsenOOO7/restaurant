package com.arsen.peter.restaurant.model;

import com.arsen.peter.project.restaurant.model.block.Block;
import com.arsen.peter.project.restaurant.model.user.User;
import com.arsen.peter.project.restaurant.service.container.map.CustomHashMap;
import com.arsen.peter.project.restaurant.service.container.map.IMap;
import com.arsen.peter.project.restaurant.service.data.user.UserStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Date;

public class BlockModelTest {

    @Test
    public void testNormal(){

        Block ban = new Block();
        ban.setId(1);
        ban.setUser_id(11);
        ban.setReason("he was rude");

        long millis = System.currentTimeMillis();
        int seconds = (int) millis / 1000;

        ban.setStart_time(seconds);
        ban.setEnd_time(seconds+3600);

        Assertions.assertEquals(1, ban.getId());
        Assertions.assertEquals(11, ban.getUser_id());
        Assertions.assertEquals("he was rude", ban.getReason());
        Assertions.assertEquals(seconds, ban.getStart_time());
        Assertions.assertEquals(seconds+3600, ban.getEnd_time());


    }

    @Test
    public void testUserBan(){
        User user = new User();
        user.setId(1);
        user.setName("Name1");
        user.setSurname("surname");
        user.setLogin("12345");
        user.setPassword("qwerty");
        user.setEmail("user@gmail.com");
        user.setBirth_date(Date.valueOf("2000-12-4"));
        user.setBalance(123212411);
        user.setStatus(UserStatus.USER);


        Block ban = new Block();
        ban.setId(1);
        ban.setUser_id(1);
        ban.setReason("he was rude");

        long millis = System.currentTimeMillis();
        int seconds = (int) millis / 1000;

        ban.setStart_time(seconds);
        ban.setEnd_time(seconds+3600);

        user.setBlock(ban);


        Assertions.assertTrue(user.isBlocked());
        Assertions.assertEquals("he was rude", user.getBlock().getReason());


    }

    @Test
    public void testExtract(){
        IMap map = new CustomHashMap();
        map.put("id", 1);
        map.put("user_id", 1);
        map.put("reason", "impolite");

        long millis = System.currentTimeMillis();
        int seconds = (int) millis / 1000;

        map.put("start_time", seconds);
        map.put("end_time", seconds+3600);



        Block ban = new Block();
        ban.extract(map);
        Assertions.assertEquals(1, ban.getId());
        Assertions.assertEquals(1, ban.getUser_id());
        Assertions.assertEquals("impolite", ban.getReason());
        Assertions.assertEquals(seconds, ban.getStart_time());
        Assertions.assertEquals(seconds+3600, ban.getEnd_time());




    }

}
