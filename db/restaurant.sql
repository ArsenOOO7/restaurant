








SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";




DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `id` int(13) NOT NULL,
  `user_id` int(13) NOT NULL,
  `table_id` int(13) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `booking_dishes`;
CREATE TABLE `booking_dishes` (
  `id` int(13) NOT NULL,
  `user_id` int(13) NOT NULL,
  `booking_id` int(13) NOT NULL,
  `dishes_id` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`dishes_id`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(13) NOT NULL,
  `identifier` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `dishes`;
CREATE TABLE `dishes` (
  `id` int(13) NOT NULL,
  `category_id` int(13) NOT NULL,
  `identifier` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `tables`;
CREATE TABLE `tables` (
  `id` int(13) NOT NULL,
  `seats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(13) NOT NULL,
  `name` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `balance` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `login` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP TABLE IF EXISTS `users_block`;
CREATE TABLE `users_block` (
  `id` int(13) NOT NULL,
  `user_id` int(13) NOT NULL,
  `reason` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` int(16) NOT NULL,
  `end_time` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







DROP VIEW IF EXISTS `users_order_view`;
CREATE TABLE `users_order_view` (
`id` int(13)
,`user_id` int(13)
,`table_id` int(13)
,`date` datetime
,`booking_dishes_id` int(13)
,`dishes` longtext
);







DROP VIEW IF EXISTS `users_view`;
CREATE TABLE `users_view` (
`id` int(13)
,`name` varchar(56)
,`surname` varchar(56)
,`email` varchar(100)
,`login` varchar(56)
,`password` varchar(56)
,`balance` int(11)
,`birth_date` date
,`status` tinyint(4)
,`blocked` int(1)
);






DROP TABLE IF EXISTS `users_order_view`;

DROP VIEW IF EXISTS `users_order_view`;
CREATE VIEW `users_order_view`  AS SELECT `booking`.`id` AS `id`, `booking`.`user_id` AS `user_id`, `booking`.`table_id` AS `table_id`, `booking`.`date` AS `date`, if(exists(select 1 from `booking_dishes` where `booking_dishes`.`user_id` = `booking`.`user_id` limit 1),(select `booking_dishes`.`id` from `booking_dishes` where `booking_dishes`.`user_id` = `booking`.`user_id`),-1) AS `booking_dishes_id`, if(exists(select 1 from `booking_dishes` where `booking_dishes`.`user_id` = `booking`.`user_id` limit 1),(select `booking_dishes`.`dishes_id` from `booking_dishes` where `booking_dishes`.`user_id` = `booking`.`user_id`),'') AS `dishes` FROM `booking` WHERE cast(`booking`.`date` as date) >= current_timestamp() ;






DROP TABLE IF EXISTS `users_view`;

DROP VIEW IF EXISTS `users_view`;
CREATE VIEW `users_view`  AS SELECT `users`.`id` AS `id`, `users`.`name` AS `name`, `users`.`surname` AS `surname`, `users`.`email` AS `email`, `users`.`login` AS `login`, `users`.`password` AS `password`, `users`.`balance` AS `balance`, `users`.`birth_date` AS `birth_date`, `users`.`status` AS `status`, if(exists(select 1 from `users_block` where `users_block`.`user_id` = `users`.`id` and `users_block`.`start_time` <= unix_timestamp() and `users_block`.`end_time` >= unix_timestamp() limit 1),1,0) AS `blocked` FROM `users` WHERE 1 ;








ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `table_id` (`table_id`);




ALTER TABLE `booking_dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_dishes_ibfk_1` (`booking_id`),
  ADD KEY `booking_dishes_ibfk_2` (`user_id`);




ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `dishes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);




ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);




ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users_block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `booking`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `booking_dishes`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `categories`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `dishes`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tables`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users_block`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT;

ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `booking_dishes`
  ADD CONSTRAINT `booking_dishes_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `booking_dishes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `dishes`
  ADD CONSTRAINT `dishes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users_block`
  ADD CONSTRAINT `users_block_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
